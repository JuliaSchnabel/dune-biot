# INSTRUCTIONS FOR WORKING WITH DUNE_BIOT

STEP 1. (in dune/dune-biot/src/biotequation_P1_3D-Dirichlet )
In the folder biotequation_P1_3D_Dirichlet is the ufl-file and the ini-file, which is needed to generate the
operators with dune-codegen. If the model is to be adapted, the values must be adapted in the ini-file. 

STEP 2. (in releasebuild/dune-biot/src/biotequation_P1_3D-Dirichlet)
To generate the operators compile with 

make 


STEP 3. (in releasebuild/dune-biot/src/biotequation_P1_3D-Dirichlet)
Copy the resulting files biot_3D_di_massOperator.hh and bioteq_r_operator.hh to 
dune/dune-biot/src/biotequation_P1_3D-Dirichlet/own


STEP 4: (in dune/dune-biot/src/biotequation_P1_3D-Dirichlet/own)
To create the grid in Zellmodell.py adjust the values as desired. 
Then run it in the terminal:

python3 Zellmodell.py

This will create an msh2-file that will be used later and an msh-file that is only meant for re-viewing the grid.
It also creates the diricletcond.txt file that specifies the areas where the 
dirichlet-conditions apply. These must (only if the model was changed) in biotequations_driver.hh
replace the corresponding places.


STEP 5. (in dune/dune-biot/src/biotequation_P1_3D-Dirichlet/own)
If required change Values in biotequations_3D.ini


STEP 6. (in releasebuild/dune-biot/src/biotequation_P1_3D-Dirichlet/own)
Compile with 

make

execute with 

./biot_3D_Di_own biotequations.ini
The oitput are vtk-files and  Files for the evaluation of -2.5*degree p at special 
points previously defined in biotequations_driver.hh.

(if Step 7 needed)
Copy Values_at_Point_**.txt to dune/dune-biot/src/biotequation_P1_3D-Dirichlet/own


STEP 7. (in dune/dune-biot/src/biotequation_P1_3D-Dirichlet/own)(ONLY for plotting the Pointvalues)
Change in evaluation_at_point.py to the desired txt file.
Then run it in the terminal:

python3 evaluation_at_point.py
