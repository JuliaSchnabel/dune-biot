#ifndef BIOTEQUATIONS_MASSOPERATOR_FILE_HH
#define BIOTEQUATIONS_MASSOPERATOR_FILE_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/typetree/childextraction.hh"
#include "dune/codegen/localbasiscache.hh"
#include "dune/pdelab/common/quadraturerules.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


template<typename GFSU, typename GFSV>
class massOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>,
      public Dune::PDELab::FullVolumePattern
{  

  public:
  massOperator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams)
  {
    fillQuadraturePointsCache(gfsu.gridView().template begin<0>()->geometry(), 4, qp_dim2_order4);
    fillQuadratureWeightsCache(gfsu.gridView().template begin<0>()->geometry(), 4, qw_dim2_order4);
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = true };
  

  public:
  using GFSV_1 = Dune::TypeTree::Child<GFSV,1>;
  

  public:
  using P2_LocalBasis = typename GFSV_1::Traits::FiniteElementMap::Traits::FiniteElementType::Traits::LocalBasisType;
  

  public:
  LocalBasisCacheWithoutReferences<P2_LocalBasis> cache_CG2;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Vector> qp_dim2_order4;
  

  public:
  using GFSV_0 = Dune::TypeTree::Child<GFSV,0>;
  

  public:
  using GFSV_0_0 = Dune::TypeTree::Child<GFSV_0,0>;
  

  public:
  using GFSV_0_1 = Dune::TypeTree::Child<GFSV_0,1>;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Field> qw_dim2_order4;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  template<typename LFSU, typename LFSV, typename X, typename EG, typename R>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    using namespace Dune::Indices;
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 4);
    auto quadrature_size = quadrature_rule.size();
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_0 = child(lfsv_0, _0);
    auto lfsv_0_0_size = lfsv_0_0.size();
    auto lfsv_0_1 = child(lfsv_0, _1);
    auto lfsv_0_1_size = lfsv_0_1.size();
    double acc_lfsv_1_0_trial_index;
    double acc_lfsv_1_0_trialgrad_index;
    double acc_lfsv_1_0_trialgrad_index_0;
    double detjac;
    double gradu_0[2];
    double gradu_1[2];
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::JacobianReturnType js_CG2;
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::FunctionReturnType phi_CG2;
    double u_2;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      detjac = cell_geo.integrationElement(qp_dim2_order4[q]);
      js_CG2 = cache_CG2.evaluateJacobian(qp_dim2_order4[q], lfsv_1.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order4[q]);
      acc_lfsv_1_0_trial_index = 0.0;
      phi_CG2 = cache_CG2.evaluateFunction(qp_dim2_order4[q], lfsv_1.finiteElement().localBasis());
      for (int lfsv_1_0_trial_index = 0; lfsv_1_0_trial_index <= -1 + lfsv_1_size; ++lfsv_1_0_trial_index)
        acc_lfsv_1_0_trial_index = x(lfsv_1, lfsv_1_0_trial_index) * (phi_CG2[lfsv_1_0_trial_index])[0] + acc_lfsv_1_0_trial_index;
      u_2 = acc_lfsv_1_0_trial_index;
      for (int idim0 = 0; idim0 <= 1; ++idim0)
      {
        acc_lfsv_1_0_trialgrad_index_0 = 0.0;
        acc_lfsv_1_0_trialgrad_index = 0.0;
        for (int lfsv_1_0_trialgrad_index = 0; lfsv_1_0_trialgrad_index <= -1 + lfsv_1_size; ++lfsv_1_0_trialgrad_index)
        {
          acc_lfsv_1_0_trialgrad_index_0 = x(lfsv_0_1, lfsv_1_0_trialgrad_index) * ((js_CG2[lfsv_1_0_trialgrad_index])[0])[idim0] + acc_lfsv_1_0_trialgrad_index_0;
          acc_lfsv_1_0_trialgrad_index = x(lfsv_0_0, lfsv_1_0_trialgrad_index) * ((js_CG2[lfsv_1_0_trialgrad_index])[0])[idim0] + acc_lfsv_1_0_trialgrad_index;
        }
        gradu_1[idim0] = acc_lfsv_1_0_trialgrad_index_0;
        gradu_0[idim0] = acc_lfsv_1_0_trialgrad_index;
      }
      for (int lfsv_1_0_index = 0; lfsv_1_0_index <= -1 + lfsv_1_size; ++lfsv_1_0_index)
        r.accumulate(lfsv_1, lfsv_1_0_index, (phi_CG2[lfsv_1_0_index])[0] * (u_2 + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]) * qw_dim2_order4[q] * detjac);
    }
  }
  

  public:
  template<typename LFSU, typename LFSV, typename X, typename EG, typename J>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    using namespace Dune::Indices;
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 4);
    auto quadrature_size = quadrature_rule.size();
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_0 = child(lfsv_0, _0);
    auto lfsv_0_0_size = lfsv_0_0.size();
    auto lfsv_0_1 = child(lfsv_0, _1);
    auto lfsv_0_1_size = lfsv_0_1.size();
    double detjac;
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::JacobianReturnType js_CG2;
    typename LocalBasisCacheWithoutReferences<P2_LocalBasis>::FunctionReturnType phi_CG2;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      phi_CG2 = cache_CG2.evaluateFunction(qp_dim2_order4[q], lfsv_1.finiteElement().localBasis());
      detjac = cell_geo.integrationElement(qp_dim2_order4[q]);
      js_CG2 = cache_CG2.evaluateJacobian(qp_dim2_order4[q], lfsv_1.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order4[q]);
      for (int lfsv_1_1_index = 0; lfsv_1_1_index <= -1 + lfsv_1_size; ++lfsv_1_1_index)
        for (int lfsv_1_0_index = 0; lfsv_1_0_index <= -1 + lfsv_1_size; ++lfsv_1_0_index)
        {
          jac.accumulate(lfsv_1, lfsv_1_0_index, lfsv_1, lfsv_1_1_index, (phi_CG2[lfsv_1_0_index])[0] * (phi_CG2[lfsv_1_1_index])[0] * qw_dim2_order4[q] * detjac);
          jac.accumulate(lfsv_1, lfsv_1_0_index, lfsv_0_1, lfsv_1_1_index, (phi_CG2[lfsv_1_0_index])[0] * ((jit[1])[0] * ((js_CG2[lfsv_1_1_index])[0])[0] + (jit[1])[1] * ((js_CG2[lfsv_1_1_index])[0])[1]) * qw_dim2_order4[q] * detjac);
          jac.accumulate(lfsv_1, lfsv_1_0_index, lfsv_0_0, lfsv_1_1_index, (phi_CG2[lfsv_1_0_index])[0] * ((jit[0])[0] * ((js_CG2[lfsv_1_1_index])[0])[0] + (jit[0])[1] * ((js_CG2[lfsv_1_1_index])[0])[1]) * qw_dim2_order4[q] * detjac);
        }
    }
  }
};


#pragma GCC diagnostic pop

#endif //BIOTEQUATIONS_MASSOPERATOR_FILE_HH
