#ifndef BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
#define BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH


#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "biotequations_3D_massOperator_file.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/codegen/vtkpredicate.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include <random>
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/pdelab/gridoperator/onestep.hh"
#include "dune/pdelab/newton/newton.hh"
#include "dune/pdelab/solver/newton.hh"
#include "dune/grid/io/file/vtk/vtksequencewriter.hh"
#include "dune/pdelab/gridfunctionspace/subspace.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh"
#include<dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/common/vtkexport.hh>

template <typename Grid, typename DGF, typename SKALAR>
class ProductGridfunctionSkalar
{
private:
    const DGF & a; //discretegridfunction
    const SKALAR b;
    typedef typename DGF::Traits::DomainType DT;
    typedef typename DGF::Traits::RangeType RT;
    typedef typename Grid::Traits::template Codim<0>::Entity Entity;

public:
    ProductGridfunctionSkalar(const DGF & a_, const SKALAR & b_)
    : a(a_), b(b_)

    { }

    void evaluate(const Entity & e, const DT &  lx, RT & y) const
    {
        RT av ;
        a.evaluate(e, lx, av);
        y = b * av;
    }
};

//template <typename Grid, typename DGF, typename SKALAR>
//void ProductGridfunctionSkalar(const DGF & a, const SKALAR b;)
//{
//    typedef typename DGF::Traits::DomainType DT;
//    typedef typename DGF::Traits::RangeType RT;
//    typedef typename Grid::Traits::template Codim<0>::Entity Entity;
//
//
//
//}

template<typename Grid, typename GV,typename FEM>
void driver(const Grid& grid, const GV& gv, FEM& fem, Dune::ParameterTree& initree, int degree)
{
    //dimesion for gv
    const int dim = GV::dimension;
    typedef double RF;
    using RangeType = double;
    using DF = typename GV::ctype;
    double time = 0.0;
    
    //GridfunctionSpaces
        //GFS für u
        using VectorBackend1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
        using GFS = Dune::PDELab::GridFunctionSpace<GV,FEM, Dune::PDELab::ConformingDirichletConstraints, VectorBackend1>; //MARK: hier nur Diriclet .... Ändern Anders für Neumann?
            //k-mal den GridfunctionSpace
        using GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<GFS, 3, VectorBackend1, Dune::PDELab::LexicographicOrderingTag>;
    
        GFS u_gfs(gv, fem);
        u_gfs.name("u");
            //GFS in u1 und u2 teilen
        GFS_POW2GFS u1u2_gfs(u_gfs);
        u1u2_gfs.child(Dune::Indices::_0).name("u1");
        u1u2_gfs.child(Dune::Indices::_1).name("u2");
        u1u2_gfs.child(Dune::Indices::_2).name("u3");
    
        //GFS für p
        GFS p_gfs(gv, fem);
        p_gfs.name("p");
    
        //Kombinieren
        using GFS_POW2GFS_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackend1, Dune::PDELab::LexicographicOrderingTag, GFS_POW2GFS, GFS>;
        //Builds a new gridfunctionspae out of the two last given Gfs
        GFS_POW2GFS_GFS combinationGFS(u1u2_gfs,p_gfs);
        combinationGFS.update();
      
    
    
    //LocalOperator
        //gleich weil conforming
        using LOCAL_OPERATOR_r = BiotEq_r_Operator<GFS_POW2GFS_GFS,GFS_POW2GFS_GFS>;
        LOCAL_OPERATOR_r lop_r(combinationGFS,combinationGFS,initree);
    //massOperator
        using LOCAL_OPERATOR_MASS = massOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS>;
        LOCAL_OPERATOR_MASS lop_mass(combinationGFS,combinationGFS, initree);
        
    
    
    //Gridfunction
    using BoolFunctionExpression = std::function<bool(typename GV::Intersection, typename GV::Intersection::LocalCoordinate)>;
    using BoolGridFunctionLeaf = Dune::PDELab::LocalCallableToBoundaryConditionAdapter<BoolFunctionExpression>;
    
    
    
    // Where dirichlet-cond lampbda function
            //location diriclecond for u TODO: ÄNDERN für verschiedene Größen
                BoolFunctionExpression diricletcondu([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[2] < 1e-08 ? 1 : 0.0);; });
            //location diriclecond for p
                BoolFunctionExpression diricletcondp([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[0] < 1e-08 or x[0] > 1 - 1e-08 or x[1] < 1e-08 or x[1] > 1 - 1e-08 ? 1 : 0.0);; });
                
        //dirichletcond u
            //dirichlet space u1
                BoolGridFunctionLeaf dirichlet_u1(diricletcondu);
            //dirichlet space u2
                BoolGridFunctionLeaf dirichlet_u2(diricletcondu);
            //dirichlet space u3
                BoolGridFunctionLeaf dirichlet_u3(diricletcondu);
            //zusammenpacken
    
        using CompositeConstraintsParameters_u1_is_u2u3 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(dirichlet_u1)>,std::decay_t<decltype(dirichlet_u2)>,std::decay_t<decltype(dirichlet_u3)>>;

        CompositeConstraintsParameters_u1_is_u2u3 compositu1u2u3(dirichlet_u1, dirichlet_u2, dirichlet_u3);
    
//            using CompositeConstraintsParameters_u1_is_u2 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(dirichlet_u1)>,std::decay_t<decltype(dirichlet_u2)>>;
//
//                CompositeConstraintsParameters_u1_is_u2 compositu1u2(dirichlet_u1,dirichlet_u2);
//
//            using CompositeConstraintsParameters_u1_is_u2_u3 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(compositu1u2)>,std::decay_t<decltype(dirichlet_u3)>>;
//
//            CompositeConstraintsParameters_u1_is_u2_u3  is_diriclet_u(compositu1u2, dirichlet_u3);
    
            //dirichlet space p
            BoolGridFunctionLeaf dirichlet_p(diricletcondp);
            
            //GESAMMTER DIRICHLETBEREICH
            using BoundaryConditionType = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(compositu1u2u3)>,std::decay_t<decltype(dirichlet_p)>>;
    
                BoundaryConditionType is_dirichlet(compositu1u2u3,dirichlet_p);
        
            //actual dirichlet cond
                //0 auf Rand
                using FunctionExpression = std::function<RangeType(typename GV::template Codim<0>::Entity, typename GV::template Codim<0>::Entity::Geometry::LocalCoordinate)>;
    
                using GridFunctionLeaf = Dune::PDELab::LocalCallableToInstationaryGridFunctionAdapter<GV, RangeType, 1, FunctionExpression, LOCAL_OPERATOR_r>;
    
    //___________________________
                FunctionExpression zeroOnBoundary([&](const auto& is, const auto& xl){ return 0.0; });
                    //u1
                    GridFunctionLeaf dBoundary_u1(gv, zeroOnBoundary, lop_r);
    
                    //u2
                    GridFunctionLeaf dBoundary_u2(gv,zeroOnBoundary, lop_r);
    
                    //u3
                    GridFunctionLeaf dBoundary_u3(gv,zeroOnBoundary, lop_r);
                //u1u2 together
                using dBOUNDARYu123 = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1)>,std::decay_t<decltype(dBoundary_u2)>, std::decay_t<decltype(dBoundary_u3)>>;
                    
                    dBOUNDARYu123 dBoundary_u(dBoundary_u1, dBoundary_u2, dBoundary_u3);
    
//                    using dBOUNDARYu = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1u2)>,std::decay_t<decltype(dBoundary_u3)>>;
//
//                    dBOUNDARYu dBoundary_u1u2u3(dBoundary_u1u2, dBoundary_u3);
                
                    //p
                    GridFunctionLeaf dBoundary_p(gv, zeroOnBoundary, lop_r);
    
                //DirichletBoundary Complete
                using BoundaryGridFunction = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u)>,std::decay_t<decltype(dBoundary_p)>>; //dBoundary_u1u2,dBoundary_p
                    BoundaryGridFunction BoundaryCondition(dBoundary_u,dBoundary_p);
   
        
    //Constrains
        //constrainscontainer
        using CONSTRAINS = typename GFS_POW2GFS_GFS::template ConstraintsContainer<RangeType>::Type;
        CONSTRAINS constrains;
        //clear
        constrains.clear();
        //Fill with constrains
        Dune::PDELab::constraints(is_dirichlet, combinationGFS, constrains);
    
    
    //Gridoperator
        combinationGFS.update();
        //DOF-estimate
        int generic_dof_estimate =  6 * combinationGFS.maxLocalSize();
        int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    
        // MatrixBeackend erstellen
        //MARK: Nicht ischer ob rihtig, aus Tutorial1 übernommen (Nur shätzung wie groß Matrix? Klappt aber aus irgend einem grund
        using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
        MBE mbe((int)pow(1+2*degree,dim));
    
        //Make Gridoperator of r
        /*
        typedef Dune::PDELab::GridOperator<
        GFS,GFS,   ansatz and test space
        LOP,       local operator
        MBE,       matrix backend
        RF,RF,RF,  domain, range, jacobian field type
        CC,CC      constraints for ansatz and test space
        > GO;
        */
        using GRIDOPERATOR_r = Dune::PDELab::GridOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS, LOCAL_OPERATOR_r, MBE, DF, RangeType, RangeType, CONSTRAINS, CONSTRAINS>;
        using GRIDOPERATOR_mass = Dune::PDELab::GridOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS, LOCAL_OPERATOR_MASS, MBE, DF, RangeType, RangeType, CONSTRAINS, CONSTRAINS>;
    
        GRIDOPERATOR_r go_r(combinationGFS, constrains, combinationGFS, constrains, lop_r, mbe);
        GRIDOPERATOR_mass go_mass(combinationGFS, constrains, combinationGFS, constrains, lop_mass, mbe);
    
        std::cout << "gfs with " << combinationGFS.size() << " dofs generated  "<< std::endl;
        std::cout << "cc with " << constrains.size() << " dofs generated  "<< std::endl;
        
        //ONE STEP GRID OPERATOR ---combines  two gridOperators representing the spartial and temporal residual forms
        // Ermöglicht sehr allgemeine Implementierung von Diskretisierungen unter VErwendung der Linienmethode
        using IGO = Dune::PDELab::OneStepGridOperator<GRIDOPERATOR_r,GRIDOPERATOR_mass>;
        
        IGO igo(go_r, go_mass);


    //vector
    using V_R = Dune::PDELab::Backend::Vector<GFS_POW2GFS_GFS,DF>;
    V_R x_r(combinationGFS);

    x_r = 0.0;
    Dune::PDELab::interpolate(BoundaryCondition, combinationGFS, x_r);
    



    //Solver
    using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_SuperLU; //Solver backend using SuperLU as a direct solver
    //using SLP = Dune::PDELab::StationaryLinearProblemSolver<GRIDOPERATOR_r, LinearSolver, V_R>;  // Nur bei stationäry
    //using SNP = Dune::PDELab::Newton<IGO, LinearSolver, V_R>;
    using SNP = Dune::PDELab::NewtonMethod<IGO,LinearSolver>;
    
    LinearSolver ls(false); //false bewirkt dass keine Massage geprintet wird
    double reduction = initree.get<double>("reduction", 1e-8);
    //SLP slp (IGO, ls, x_r, reduction); // Nur bei stationäry
    //SNP snp (igo,x_r,ls);
    SNP snp(igo,ls);
    snp.setParameters(initree.sub("newton"));
    
    
    //INSTATIONARY STUFF --time
        //Parameters to turn the OneStepMethod into an Alexander scheme.
        //Prepare an select time-stepping sheme
        //https://dune-project.org/doxygen/pdelab/releases2.6/group__OneStepMethod.html
        //change to method we want to use
//        using TSM = Dune::PDELab::Alexander2Parameter<RangeType>;
        using TSM = Dune::PDELab::ImplicitEulerParameter<RangeType>;
        TSM tsm;
        tsm.name();
        //ONE STEP METHOD - do one step of a time stepping sheme 
        using OSM = Dune::PDELab::OneStepMethod<RangeType, IGO, SNP, V_R, V_R>;
        OSM osm(tsm, igo, snp);
    
    using VTKSW = Dune::VTKSequenceWriter<GV>;
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    int subsampling = initree.get("subsampling.subsampling", (int) 1);  // last Parameter default Value
    Dune::RefinementIntervals subint(subsampling);
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = initree.get<std::string>("wrapper.vtkcompare.name", "output");
    VTKSW vtkSequenceWriter(std::make_shared<VTKWriter>(vtkwriter), vtkfile);
    CuttingPredicate predicate;
    Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter, combinationGFS , x_r, Dune::PDELab::vtk::defaultNameScheme(), predicate);
    
    
        //p aus berechnetem als grid function
    using Pathp = Dune::TypeTree::HybridTreePath<Dune::index_constant<1>>;
    using PSubGFS = Dune::PDELab::GridFunctionSubSpace<GFS_POW2GFS_GFS, Pathp>;
    PSubGFS psubGFS(combinationGFS);
    
    using PDGF = Dune::PDELab::DiscreteGridFunction<PSubGFS, V_R>;
    PDGF p_dgf(psubGFS, x_r);
    vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<PDGF>>(p_dgf,"p_test"));
        
        //grad p
    using GRAD_P_GF = Dune::PDELab::DiscreteGridFunctionGradient<PSubGFS,V_R>;
    GRAD_P_GF grad_p_dgf(psubGFS, x_r);
    vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<GRAD_P_GF>>(grad_p_dgf,"grad_p_test"));
        
        //Darcy-flow
    double kappa = initree.get<int>("constants.kappa", 1);
    double eta = initree.get<int>("constants.eta", 1);
    
    //TODO: DARCYFLOW implementieren
    using DARCYFLOW_MULTIPLY = ProductGridfunctionSkalar<Grid, GRAD_P_GF, RF>;
    DARCYFLOW_MULTIPLY darcy_dgf(grad_p_dgf, -1.0);
//    vtkSequenceWriter.addVertexData( Dune::PDELab::VTKGridFunctionAdapter<DARCYFLOW_MULTIPLY>(darcy_dgf,"darcy_test"));
        
    
    
    
                                    
   //U als vektor ausgeben --vlt funktioniert iwie nicht
    using Pathu = Dune::TypeTree::HybridTreePath<Dune::index_constant<0>>;
    using USubGFS = Dune::PDELab::GridFunctionSubSpace<GFS_POW2GFS_GFS, Pathu>;
    USubGFS usubGFS(combinationGFS);
    
    using UDGF = Dune::PDELab::VectorDiscreteGridFunction<USubGFS, V_R>;
    UDGF u_dgf(usubGFS, x_r);
    vtkSequenceWriter.addVertexData(std::make_shared< Dune::PDELab::VTKGridFunctionAdapter<UDGF>>(u_dgf,"u_test"));
    
    //vtkwriter.write(vtkfile, Dune::VTK::ascii);
    vtkSequenceWriter.write(time, Dune::VTK::appendedraw);
    
    //solve instationary Problem
    
    double T = initree.get<double>("instat.T", 1.0);
    double dt = initree.get<double>("instat.dt", 0.1);
    int stepNumber(0);
    int output_every_nth = initree.get<int>("instat.nth", 1);
    
    
    while(time <T-1e-8){
        //constrains for next step
        lop_r.setTime(time+dt);
        Dune::PDELab::constraints(is_dirichlet, combinationGFS , constrains);
            
        //Do time step
        V_R new_coefficient(x_r);
        osm.apply(time, dt, x_r, BoundaryCondition, new_coefficient);
        
        //Acept new time step
        x_r = new_coefficient;
        time += dt;
        
        
        stepNumber += 1;
        if(stepNumber % output_every_nth == 0){
            vtkSequenceWriter.vtkWriter()->clear();
            Dune::PDELab::addSolutionToVTKWriter(vtkSequenceWriter, combinationGFS, x_r ,Dune::PDELab::vtk::defaultNameScheme(), predicate);
            vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<PDGF>>(p_dgf,"p_test"));
            GRAD_P_GF grad_p_dgf(psubGFS, x_r);
            vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<GRAD_P_GF>>(grad_p_dgf,"grad_p_test"));

            UDGF u_dgf(usubGFS, x_r);
            vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<UDGF>>(u_dgf,"u_test"));


            vtkSequenceWriter.write(time, Dune::VTK::appendedraw);
        }
        
    }
    
    

}

#endif //BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH

