#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
 
#include <iostream>
 
#include <dune/common/filledarray.hh>
#include <dune/common/parallel/mpihelper.hh>
 
#include <dune/grid/yaspgrid.hh>
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "dune/grid/io/file/vtk/vtksequencewriter.hh"
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab.hh>
template<class GridView, class Functor>
void writeVTKFile(const GridView & gridview, const Functor & f, std::string filename)
{
  Dune::SubsamplingVTKWriter<GridView> vtkwriter(gridview,Dune::refinementLevels(0));
  vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<Functor>>(f,"test"));
  vtkwriter.write(filename, Dune::VTK::appendedraw);
  std::cout << "Wrote VTK file" << std::endl;
}

//
template<typename GF>
void integrateGridFunction_1(const GF& gf,
                                typename GF::Traits::RangeType& sum,
                                unsigned qorder = 1) {
       typedef typename GF::Traits::GridViewType GV;
       typedef typename GV::template Codim<0>::Geometry Geometry;
       typedef typename GF::Traits::RangeType Range;
       typedef typename GF::Traits::DomainFieldType DF;
       static const int dimD = GF::Traits::dimDomain;
       typedef Dune::QuadratureRule<DF,dimD> QR;
       typedef Dune::QuadratureRules<DF,dimD> QRs;

       sum = 0;
       Range val;
    int i = 1;
       for(const auto& cell : elements(gf.getGridView())) {
         const Geometry& geo = cell.geometry();
         Dune::GeometryType gt = geo.type();
         const QR& rule = QRs::rule(gt,qorder);
         for (const auto& qip : rule) {
           // evaluate the given grid functions at integration point
             std::cout << "--------------------------------------qp: " << qip.position() << std::endl;
           gf.evaluate(cell,qip.position(),val);
             std::cout << i<<std::endl;
             auto x_global = geo.global(qip.position());
             std::cout << "punkt " << x_global << std::endl;
           // accumulate error
           val *= qip.weight() * geo.integrationElement(qip.position());
          sum += val;
             std::cout << "sum " << sum << std::endl;

             i = i+1;
         }
       }
    }

template<typename GF>
void integrateGridFunction_boundary(const GF& gf,
                                typename GF::Traits::RangeType& sum,
                                unsigned qorder = 1) {
       typedef typename GF::Traits::GridViewType GV;
       typedef typename GV::template Codim<0>::Geometry Geometry;
       typedef typename GF::Traits::RangeType Range;
       typedef typename GF::Traits::DomainFieldType DF;
       static const int dimD = GF::Traits::dimDomain;
       typedef Dune::QuadratureRule<DF,dimD> QR;
       typedef Dune::QuadratureRules<DF,dimD> QRs;

       sum = 0;
       Range val;
    int i = 1;
       for(const auto& cell : elements(gf.getGridView())) {
         const Geometry& geo = cell.geometry();
         Dune::GeometryType gt = geo.type();
//           int corner = geo.corners();
//           std::cout << "corners " << corner << std::endl;
//           auto corner_1 = geo.corner(0);
//           std::cout << "corner_1 " << corner_1 << std::endl;
           i = i+1;
           for(const auto& intersection : intersections(gf.getGridView(),cell)){
//            for(const auto& intersection : intersections(gf.getGridView(), *gf.getGridView().template begin<0>())){
            // Geometry of intersection
               auto ig = intersection.geometry();
               
           
               constexpr auto intersectionDim = decltype(ig)::mydimension;
//               std::cout << "dimtype " << intersectionDim << std::endl;
               
            //Is there a interseczion with domainboundary?
               bool b = intersection.boundary();
               std::cout << "intersection with domain boundary " << b << std::endl;
            //Center of intersections in global coordinates
               Dune::FieldVector<DF, dimD> intersectionCenter = ig.center();
//               auto local= localgeo.local(intersectionCenter);
               std::cout << "punkt_mitte_test " << intersectionCenter << std::endl;

//               const QR& rule = QRs::rule(gt,qorder);
               const auto& quadRule = Dune::QuadratureRules<double,intersectionDim>::rule(ig.type(),
               qorder);
               
               for (const auto& quadPoint :quadRule){
                   if(b == false)
                   {
                       std::cout << "not Boundary: " << b << std::endl;
                       continue;
                   }
                   auto wert = ig.global(quadPoint.position());
                   std::cout << "--------------------------------------wert: " << wert << std::endl;
                   std::cout << "--------------------------------------qp: " << quadPoint.position() << std::endl;
                   
                   gf.evaluate(cell,wert,val);
                   
                   val *= quadPoint.weight() * ig.integrationElement(quadPoint.position());
                   
                   std::cout << "VAL: " << val << std::endl;
//                   auto quadPointLocalIn = geometryInInside.global(quadPoint.position());
                   sum += val;
                   
               }
               std::cout << "------------------------SUM: " << sum << std::endl;
               
               
//               for (const auto& qip : rule) {
//                   auto x_global = geo.global(qip.position());
//                   std::cout << "qip " << x_global << std::endl;}
                 // evaluate the given grid functions at integration point
//                   if(qip.position()[0] == 0 or qip.position()[1] == 0){
//                   gf.evaluate(cell,qip.position(),val);
                   
//                   std::cout << "punkt_mitte_test " << intersectionCenter << std::endl;

                 // accumulate error
//                 val *= qip.weight() * intersectionGEO.integrationElement(qip.position());
//                sum += val;
//                   std::cout << "sum " << sum << std::endl;
//                   std::cout << i<<std::endl;

         }
       }
    }





int main(int argc, char** argv)
{
  // Initialize Mpi
  Dune::MPIHelper::instance(argc, argv);
    
  // need a grid in order to test grid functions
  constexpr unsigned int dim = 2;
  Dune::FieldVector<double,dim> L(1.0);
  std::array<int,dim> N(Dune::filledArray<dim,int>(4));
 
  typedef Dune::YaspGrid<dim> Grid;
  Grid grid(L,N);
 
    // [Defining an analytic grid function]
    auto analyticFunction = Dune::PDELab::makeGridFunctionFromCallable(grid.leafGridView(), [&](const auto& x){
      return x[1]*x[0];
    });
    
    //vtkdatei ausgeben
    
    writeVTKFile(grid.leafGridView(), analyticFunction, "test");
 
  // [Compute integral]
//  auto integral = Dune::PDELab::integrateGridFunction(analyticFunction,10);
//
//
//  std::cout << "Integral: " << integral << std::endl;
//
//
    
    decltype (analyticFunction)::Traits::RangeType sum;
//
//
    integrateGridFunction_1(analyticFunction, sum,1);
//
//    std::cout << "Integral: " << sum << std::endl;
    std::cout << "---------------------------------" << std::endl;
    
    
    integrateGridFunction_boundary(analyticFunction, sum,1);
    

//  Integral über gesammten Rand


}
