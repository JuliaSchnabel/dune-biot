#ifndef BIOTEQ_R_OPERATOR_HH
#define BIOTEQ_R_OPERATOR_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/codegen/localbasiscache.hh"
#include "dune/typetree/childextraction.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


template<typename GFSU, typename GFSV>
class BiotEq_r_Operator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern,
      public Dune::PDELab::FullBoundaryPattern
{  

  public:
  BiotEq_r_Operator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams)
  {
    fillQuadraturePointsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 1, qp_dim1_order1);
    fillQuadratureWeightsCache(gfsu.gridView().ibegin(*(gfsu.gridView().template begin<0>()))->geometry(), 1, qw_dim1_order1);
    fillQuadraturePointsCache(gfsu.gridView().template begin<0>()->geometry(), 0, qp_dim2_order0);
    fillQuadratureWeightsCache(gfsu.gridView().template begin<0>()->geometry(), 0, qw_dim2_order0);
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 1>::Vector> qp_dim1_order1;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 1>::Field> qw_dim1_order1;
  

  public:
  using GFSV_0 = Dune::TypeTree::Child<GFSV,0>;
  

  public:
  using GFSV_0_0 = Dune::TypeTree::Child<GFSV_0,0>;
  

  public:
  using P1_LocalBasis = typename GFSV_0_0::Traits::FiniteElementMap::Traits::FiniteElementType::Traits::LocalBasisType;
  

  public:
  LocalBasisCacheWithoutReferences<P1_LocalBasis> cache_CG1;
  

  public:
  using GFSV_0_1 = Dune::TypeTree::Child<GFSV_0,1>;
  

  public:
  enum { doPatternBoundary = true };
  

  public:
  enum { doAlphaBoundary = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Vector> qp_dim2_order0;
  

  public:
  using GFSV_1 = Dune::TypeTree::Child<GFSV,1>;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Field> qw_dim2_order0;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  template<typename LFSU, typename J, typename IG, typename X, typename LFSV>
  void jacobian_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, J& jac_s_s) const
  {
  }
  

  public:
  template<typename LFSU, typename IG, typename R, typename X, typename LFSV>
  void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s) const
  {
    auto is_geo = ig.geometry();
    const auto quadrature_rule = quadratureRule(is_geo, 1);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_s_0 = child(lfsv_s, _0);
    auto lfsv_s_0_0 = child(lfsv_s_0, _0);
    auto lfsv_s_0_0_size = lfsv_s_0_0.size();
    auto geo_in_inside = ig.geometryInInside();
    auto lfsv_s_0_1 = child(lfsv_s_0, _1);
    auto lfsv_s_0_1_size = lfsv_s_0_1.size();
    double fdetjac;
    typename LocalBasisCacheWithoutReferences<P1_LocalBasis>::FunctionReturnType phi_CG1_s;
    Dune::FieldVector<double, 2> qp_dim1_order1_global(0.0);
    Dune::FieldVector<double, 2> qp_dim1_order1_in_inside(0.0);
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      qp_dim1_order1_in_inside = geo_in_inside.global(qp_dim1_order1[q]);
      phi_CG1_s = cache_CG1.evaluateFunction(qp_dim1_order1_in_inside, lfsv_s_0_0.finiteElement().localBasis());
      fdetjac = is_geo.integrationElement(qp_dim1_order1[q]);
      qp_dim1_order1_global = is_geo.global(qp_dim1_order1[q]);
      if ((qp_dim1_order1_global[1] > 1e-08 ? 0.0 : 1.0) == 0.0)
        for (int lfsv_s_0_0_0_index = 0; lfsv_s_0_0_0_index <= -1 + lfsv_s_0_0_size; ++lfsv_s_0_0_0_index)
          r_s.accumulate(lfsv_s_0_1, lfsv_s_0_0_0_index, -1.0 * fdetjac * qw_dim1_order1[q] * (phi_CG1_s[lfsv_s_0_0_0_index])[0] * (qp_dim1_order1_global[1] > 0.99999999 ? -40.0 : 0.0));
    }
  }
  

  public:
  template<typename EG, typename LFSU, typename R, typename X, typename LFSV>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 0);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_0 = child(lfsv_0, _0);
    auto lfsv_0_0_size = lfsv_0_0.size();
    auto lfsv_0_1 = child(lfsv_0, _1);
    auto lfsv_0_1_size = lfsv_0_1.size();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    double acc_lfsv_0_0_0_trialgrad_index;
    double acc_lfsv_0_0_0_trialgrad_index_0;
    double acc_lfsv_0_0_0_trialgrad_index_1;
    double detjac;
    double gradu_0[2];
    double gradu_1[2];
    double gradu_2[2];
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P1_LocalBasis>::JacobianReturnType js_CG1;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      detjac = cell_geo.integrationElement(qp_dim2_order0[q]);
      js_CG1 = cache_CG1.evaluateJacobian(qp_dim2_order0[q], lfsv_0_0.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order0[q]);
      for (int idim0 = 0; idim0 <= 1; ++idim0)
      {
        acc_lfsv_0_0_0_trialgrad_index_1 = 0.0;
        acc_lfsv_0_0_0_trialgrad_index_0 = 0.0;
        acc_lfsv_0_0_0_trialgrad_index = 0.0;
        for (int lfsv_0_0_0_trialgrad_index = 0; lfsv_0_0_0_trialgrad_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_0_trialgrad_index)
        {
          acc_lfsv_0_0_0_trialgrad_index_1 = x(lfsv_1, lfsv_0_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_0_trialgrad_index_1;
          acc_lfsv_0_0_0_trialgrad_index_0 = x(lfsv_0_1, lfsv_0_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_0_trialgrad_index_0;
          acc_lfsv_0_0_0_trialgrad_index = x(lfsv_0_0, lfsv_0_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_0_trialgrad_index;
        }
        gradu_2[idim0] = acc_lfsv_0_0_0_trialgrad_index_1;
        gradu_1[idim0] = acc_lfsv_0_0_0_trialgrad_index_0;
        gradu_0[idim0] = acc_lfsv_0_0_0_trialgrad_index;
      }
      for (int lfsv_0_0_0_index = 0; lfsv_0_0_0_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_0_index)
      {
        r.accumulate(lfsv_1, lfsv_0_0_0_index, (((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_2[0] + (jit[0])[1] * gradu_2[1]) + ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_2[0] + (jit[1])[1] * gradu_2[1])) * qw_dim2_order0[q] * detjac);
        r.accumulate(lfsv_0_1, lfsv_0_0_0_index, (224.93813695072427 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]) + 225.00562514062852 * (0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1] + (jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1] + (jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]))) * qw_dim2_order0[q] * detjac);
        r.accumulate(lfsv_0_0, lfsv_0_0_0_index, (224.93813695072427 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]) + 225.00562514062852 * (0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1] + (jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1] + (jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1]))) * qw_dim2_order0[q] * detjac);
      }
    }
  }
  

  public:
  template<typename EG, typename LFSV, typename LFSU, typename X, typename J>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 0);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_0 = child(lfsv_0, _0);
    auto lfsv_0_0_size = lfsv_0_0.size();
    auto lfsv_0_1 = child(lfsv_0, _1);
    auto lfsv_0_1_size = lfsv_0_1.size();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    double detjac;
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P1_LocalBasis>::JacobianReturnType js_CG1;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      detjac = cell_geo.integrationElement(qp_dim2_order0[q]);
      js_CG1 = cache_CG1.evaluateJacobian(qp_dim2_order0[q], lfsv_0_0.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order0[q]);
      for (int lfsv_0_0_1_index = 0; lfsv_0_0_1_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_1_index)
        for (int lfsv_0_0_0_index = 0; lfsv_0_0_0_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_0_index)
        {
          jac.accumulate(lfsv_1, lfsv_0_0_0_index, lfsv_1, lfsv_0_0_1_index, (((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1])) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_1, lfsv_0_0_0_index, lfsv_0_1, lfsv_0_0_1_index, (224.93813695072427 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1] + (jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_1, lfsv_0_0_0_index, lfsv_0_0, lfsv_0_0_1_index, (224.93813695072427 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_0, lfsv_0_0_0_index, lfsv_0_1, lfsv_0_0_1_index, (224.93813695072427 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_0, lfsv_0_0_0_index, lfsv_0_0, lfsv_0_0_1_index, (224.93813695072427 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1] + (jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
        }
    }
  }
};


#pragma GCC diagnostic pop

#endif //BIOTEQ_R_OPERATOR_HH
