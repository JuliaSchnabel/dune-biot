#ifndef BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
#define BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH


#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/codegen/vtkpredicate.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include <random>
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"

#include "dune/grid/io/file/vtk/vtksequencewriter.hh"
#include "dune/pdelab/gridfunctionspace/subspace.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh"
#include<dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/common/vtkexport.hh>


template<typename GV,typename FEM>
void driver(const GV& gv, FEM& fem, Dune::ParameterTree& initree, int degree)
{
    //dimesion for gv
    const int dim = GV::dimension;
    typedef double RF;
    using RangeType = double;
    using DF = typename GV::ctype;
    
    //GridfunctionSpaces
        //GFS für u
        using VectorBackend1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
        using GFS = Dune::PDELab::GridFunctionSpace<GV,FEM, Dune::PDELab::ConformingDirichletConstraints, VectorBackend1>; //MARK: hier nur Diriclet .... Ändern Anders für Neumann?
            //k-mal den GridfunctionSpace
        using GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<GFS, 2, VectorBackend1, Dune::PDELab::LexicographicOrderingTag>;
    
        GFS u_gfs(gv, fem);
        u_gfs.name("u");
            //GFS in u1 und u2 teilen
        GFS_POW2GFS u1u2_gfs(u_gfs);
        u1u2_gfs.child(Dune::Indices::_0).name("u1");
        u1u2_gfs.child(Dune::Indices::_1).name("u2");
    
        //GFS für p
        GFS p_gfs(gv, fem);
        p_gfs.name("p");
    
        //Kombinieren
        using GFS_POW2GFS_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackend1, Dune::PDELab::LexicographicOrderingTag, GFS_POW2GFS, GFS>;
        //Builds a new gridfunctionspae out of the two last givven Gfs
        GFS_POW2GFS_GFS combinationGFS(u1u2_gfs,p_gfs);
        combinationGFS.update();
      
    
    
    //LocalOperator
        //gleich weil conforming
        using LOCAL_OPERATOR = BiotEq_r_Operator<GFS_POW2GFS_GFS,GFS_POW2GFS_GFS>;
        LOCAL_OPERATOR lop_r(combinationGFS,combinationGFS,initree);
    
    //Gridfunction
    using BoolFunctionExpression = std::function<bool(typename GV::Intersection, typename GV::Intersection::LocalCoordinate)>;
    using BoolGridFunctionLeaf = Dune::PDELab::LocalCallableToBoundaryConditionAdapter<BoolFunctionExpression>;
    
    // Where dirichlet-cond lampbda function
            //location diriclecond for u
                BoolFunctionExpression diricletcondu1([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 1e-08 ? 1 : 0.0);; });
                BoolFunctionExpression diricletcondu2([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 1e-08 || x[1] > 0.99999999 ? 1 : 0.0);; });
            //location diriclecond for p
                BoolFunctionExpression diricletcondp([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[0] < 1e-08 || x[0] > 0.99999999 ? 1 : 0.0);; });
                
        //dirichletcond u
            //dirichlet space u1
                BoolGridFunctionLeaf dirichlet_u1(diricletcondu1);
            //dirichlet space u2
                BoolGridFunctionLeaf dirichlet_u2(diricletcondu2);
            //zusammenpacken
            using CompositeConstraintsParameters_u1_is_u2 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(dirichlet_u1)>,std::decay_t<decltype(dirichlet_u2)>>;
                CompositeConstraintsParameters_u1_is_u2  is_diriclet_u(dirichlet_u1,dirichlet_u2);
            //dirichlet space p
            BoolGridFunctionLeaf dirichlet_p(diricletcondp);
            
            //GESAMMTER DIRICHLETBEREICH
            using BoundaryConditionType = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(is_diriclet_u)>,std::decay_t<decltype(dirichlet_p)>>;
    
                BoundaryConditionType is_dirichlet(is_diriclet_u,dirichlet_p);
        
            //actual dirichlet cond
                //0 auf Rand
                using FunctionExpression = std::function<RangeType(typename GV::template Codim<0>::Entity, typename GV::template Codim<0>::Entity::Geometry::LocalCoordinate)>;
    
                using GridFunctionLeaf = Dune::PDELab::LocalCallableToGridFunctionAdapter<GV, RangeType, 1, FunctionExpression>;
                
                FunctionExpression zeroOnBoundary([&](const auto& is, const auto& xl){ return 0.0; });
                    //u1
                    GridFunctionLeaf dBoundary_u1(gv, zeroOnBoundary);
    FunctionExpression pBoundary([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[0] < 0.1 ? 0.0 : 1.0); });
                //runter drücken auf Rand
                FunctionExpression pushdown([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 0.1 ? 0.0 : -0.1); });
                    //u2
                    GridFunctionLeaf dBoundary_u2(gv,pushdown);
                //u1u2 together
                using dBOUNDARYu = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1)>,std::decay_t<decltype(dBoundary_u2)>>;
                    
                    dBOUNDARYu dBoundary_u1u2(dBoundary_u1, dBoundary_u2);
                
                //TODO: WIEDER RAUSNEHEMEN!!!!!!!!
                    //p
                    GridFunctionLeaf dBoundary_p(gv, pBoundary);
    
                //DirichletBoundary Complete
                using BoundaryGridFunction = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1u2)>,std::decay_t<decltype(dBoundary_p)>>; //dBoundary_u1u2,dBoundary_p
                    BoundaryGridFunction BoundaryCondition(dBoundary_u1u2,dBoundary_p);
    //TODO: Noch Neumannonditions Implementieren!!!
        
    //Constrains
        //constrainscontainer
        using CONSTRAINS = typename GFS_POW2GFS_GFS::template ConstraintsContainer<RangeType>::Type;
        CONSTRAINS constrains;
        //clear
        constrains.clear();
        //Fill with constrains
        Dune::PDELab::constraints(is_dirichlet, combinationGFS, constrains);
    
    
    //Gridoperator
        combinationGFS.update();
        //DOF-estimate
        int generic_dof_estimate =  6 * combinationGFS.maxLocalSize();
        int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    
        // MatrixBeackend erstellen
        //MARK: Nicht ischer ob rihtig, aus Tutorial1 übernommen (Nur shätzung wie groß Matrix? Klappt aber aus irgend einem grund
        using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
        MBE mbe((int)pow(1+2*degree,dim));
    
        //Make Gridoperator of r
        /*
        typedef Dune::PDELab::GridOperator<
        GFS,GFS,   ansatz and test space
        LOP,       local operator
        MBE,       matrix backend
        RF,RF,RF,  domain, range, jacobian field type
        CC,CC      constraints for ansatz and test space
        > GO;
        */
        using GRIDOPERATOR_r = Dune::PDELab::GridOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS, LOCAL_OPERATOR, MBE, DF, RangeType, RangeType, CONSTRAINS, CONSTRAINS>;
    
        GRIDOPERATOR_r go_r(combinationGFS, constrains, combinationGFS, constrains, lop_r, mbe);
    
        std::cout << "gfs with " << combinationGFS.size() << " dofs generated  "<< std::endl;
        std::cout << "cc with " << constrains.size() << " dofs generated  "<< std::endl;

    //vector
    using V_R = Dune::PDELab::Backend::Vector<GFS_POW2GFS_GFS,DF>;
    V_R x_r(combinationGFS);

    x_r = 0.0;
    Dune::PDELab::interpolate(BoundaryCondition, combinationGFS, x_r);
    



    //Solver
    using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_SuperLU; //Solver backend using SuperLU as a direct solver
    using SLP = Dune::PDELab::StationaryLinearProblemSolver<GRIDOPERATOR_r, LinearSolver, V_R>;
    //TODO: OBERES BEI INSTATIONARY NEWTON
    
    LinearSolver ls(false); //false bewirkt dass keine Massage geprintet wird
    double reduction = initree.get<double>("reduction", 1e-12);
    SLP slp (go_r, ls, x_r, reduction);
    
    slp.apply();
    
    //VTKWriter
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    int subsampling = initree.get("subsampling.subsampling", (int) 1);  // last Parameter default Value
    Dune::RefinementIntervals subint(subsampling);
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = initree.get<std::string>("wrapper.vtkcompare.name", "output");
    CuttingPredicate predicate;
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter, combinationGFS , x_r, Dune::PDELab::vtk::defaultNameScheme(), predicate);
  
    
    //Div p
        //p aus berechnetem als grid function
    using Pathp = Dune::TypeTree::HybridTreePath<Dune::index_constant<1>>;
    using PSubGFS = Dune::PDELab::GridFunctionSubSpace<GFS_POW2GFS_GFS, Pathp>;
    PSubGFS psubGFS(combinationGFS);
    
    using PDGF = Dune::PDELab::DiscreteGridFunction<PSubGFS, V_R>;
    PDGF p_dgf(psubGFS, x_r);
    vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<PDGF>>(p_dgf,"p_test"));
        
        //grad p
    using GRAD_P_GF = Dune::PDELab::DiscreteGridFunctionGradient<PSubGFS,V_R>;
    GRAD_P_GF grad_p_dgf(psubGFS, x_r);
    vtkwriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<GRAD_P_GF>>(grad_p_dgf,"grad_p_test"));
    
    //U als vektor ausgeben --vlt funktioniert iwie nicht
     using Pathu = Dune::TypeTree::HybridTreePath<Dune::index_constant<0>>;
     using USubGFS = Dune::PDELab::GridFunctionSubSpace<GFS_POW2GFS_GFS, Pathu>;
     USubGFS usubGFS(combinationGFS);
     
     using UDGF = Dune::PDELab::VectorDiscreteGridFunction<USubGFS, V_R>;
     UDGF u_dgf(usubGFS, x_r);
     vtkwriter.addVertexData(std::make_shared< Dune::PDELab::VTKGridFunctionAdapter<UDGF>>(u_dgf,"u_test"));

    vtkwriter.write(vtkfile, Dune::VTK::ascii);
}
#endif //BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
