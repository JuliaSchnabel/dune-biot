dune_add_generated_executable(TARGET biotequations_stationary
                              UFLFILE biotequations_stationary.ufl 
                              INIFILE biotequations_stationary.ini)

add_executable(biot_stationary biotequations_stationary_mainfunction.cc)

dune_symlink_to_source_files(FILES biotequations_stationary.ini)