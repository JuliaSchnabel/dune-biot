
#include "config.h"
#include "dune/common/parallel/mpihelper.hh"
#include "dune/alugrid/grid.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include <random>
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/codegen/vtkpredicate.hh"
#include "biotequations_stationary_driverblock.hh"



int main(int argc, char** argv){  
  try
  {    

    if (argc != 2){
      std::cerr << "This program needs to be called with an ini file" << std::endl;
      return 1;
    }
    
    // Initialize basic stuff...    
    Dune::MPIHelper& mpihelper = Dune::MPIHelper::instance(argc, argv);
    Dune::ParameterTree initree;
    Dune::ParameterTreeParser::readINITree(argv[1], initree);
    
    // Setup grid (view)...    
    using Grid = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
    using GV = Grid::LeafGridView;
    IniGridFactory<Grid> factory(initree);
    std::shared_ptr<Grid> grid = factory.getGrid();
    GV gv = grid->leafGridView();
    
    // Set up driver block...    
    DriverBlock<GV> driverBlockDefault_driver_block(gv, initree);
    auto solver = driverBlockDefault_driver_block.getSolver();
    auto gridOperator = driverBlockDefault_driver_block.getGridOperator();
    auto coefficient = driverBlockDefault_driver_block.getCoefficient();
    using Coefficient = DriverBlock<GV>::V_R;
    using GridOperator = DriverBlock<GV>::GO_r;
    auto gridFunctionSpace = driverBlockDefault_driver_block.getGridFunctionsSpace();
    
    // Set up (non)linear solvers...    
    solver->apply();
    
    // Do visualization...    
    using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
    Dune::RefinementIntervals subint(initree.get<int>("vtk.subsamplinglevel", 1));
    VTKWriter vtkwriter(gv, subint);
    std::string vtkfile = initree.get<std::string>("wrapper.vtkcompare.name", "output");
    CuttingPredicate predicate;
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter, *gridFunctionSpace, *coefficient, Dune::PDELab::vtk::defaultNameScheme(), predicate);
    vtkwriter.write(vtkfile, Dune::VTK::ascii);
    
    // Maybe print residuals and matrices to stdout...    --- Nur zum Printen
    if (initree.get<bool>("printresidual", false)) {
      using Dune::PDELab::Backend::native;
      Coefficient r(*coefficient);
      // Setup random input
      std::size_t seed = 0;
      auto rng = std::mt19937_64(seed);
      auto dist = std::uniform_real_distribution<>(-1., 1.);
      for (auto& v : *coefficient)
        v = dist(rng);
      r=0.0;
      gridOperator->residual(*coefficient, r);
      Dune::printvector(std::cout, native(r), "residual vector", "row");
    }
    if (initree.get<bool>("printmatrix", false)) {
      using Dune::PDELab::Backend::native;
      Coefficient r(*coefficient);
      // Setup random input
      std::size_t seed = 0;
      auto rng = std::mt19937_64(seed);
      auto dist = std::uniform_real_distribution<>(-1., 1.);
      for (auto& v : *coefficient)
        v = dist(rng);
      using M = typename GridOperator::Traits::Jacobian;
      M m(*gridOperator);
      gridOperator->jacobian(*coefficient,m);
      using Dune::PDELab::Backend::native;
      Dune::printmatrix(std::cout, native(m),"global stiffness matrix","row",9,1);
    }
    
    // Maybe calculate errors for test results...    
    bool testfail(false);
    
    // Return statement...    
    return testfail;
    
  }  
  catch (Dune::Exception& e)
  {    std::cerr << "Dune reported error: " << e << std::endl;
    return 1;
  }  
  catch (std::exception& e)
  {    std::cerr << "Unknown exception thrown!" << std::endl;
    return 1;
  }  
}

