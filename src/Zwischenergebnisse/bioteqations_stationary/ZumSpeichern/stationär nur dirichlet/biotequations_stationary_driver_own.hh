#ifndef BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
#define BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH


#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/constraints/conforming.hh"



template<typename GV,typename CG1_FEM>
void driver(const GV& gv, CG1_FEM& cg1_fem, Dune::ParameterTree& initree, int degree)
{
    //dimesion for gv
    const int dim = GV::dimension;
    typedef double RF;
    using RangeType = double;
    using DF = typename GV::ctype;
    
    //GridfunctionSpaces
        //GFS für u
        using VectorBackendCG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
        using CG1_GFS = Dune::PDELab::GridFunctionSpace<GV,CG1_FEM, Dune::PDELab::ConformingDirichletConstraints, VectorBackendCG1>; //MARK: hier nur Diriclet .... Ändern
        using VectorBackendCG1_2 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;//TODO: ÄNDERN WEIL WAHRSCHEINLICH UNNÖTIG gleich mit oberem
            //k-mal den GridfunctionSpace
        using CG1_GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<CG1_GFS, 2, VectorBackendCG1_2, Dune::PDELab::LexicographicOrderingTag>;
    
        CG1_GFS u_gfs(gv, cg1_fem); //*cg1_fem
        u_gfs.name("u");
            //GFS in u1 und u2 teilen
        CG1_GFS_POW2GFS u1u2_gfs(u_gfs); //*
        u1u2_gfs.child(Dune::Indices::_0).name("u1");
        u1u2_gfs.child(Dune::Indices::_1).name("u2");
    
        //GFS für p
        CG1_GFS p_gfs(gv, cg1_fem); //-,*
        p_gfs.name("p");
    
        //Kombinieren
        using VectorBackendCG1_2_CG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
            //TODO: gleich wie oben
        using CG1_GFS_POW2GFS_CG1_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackendCG1_2_CG1, Dune::PDELab::LexicographicOrderingTag, CG1_GFS_POW2GFS, CG1_GFS>;
        //Builds a new gridfunctionspae out of the two last givven Gfs
        CG1_GFS_POW2GFS_CG1_GFS combinationGFS(u1u2_gfs,p_gfs);//*,*
        combinationGFS.update();
      
    
    
    //LocalOperator
        //gleich weil conforming
        using LOCAL_OPERATOR = BiotEq_r_Operator<CG1_GFS_POW2GFS_CG1_GFS,CG1_GFS_POW2GFS_CG1_GFS>;
        LOCAL_OPERATOR lop_r(combinationGFS,combinationGFS,initree); //*,*,-

    //Gridfunction
    using BoolFunctionExpression = std::function<bool(typename GV::Intersection, typename GV::Intersection::LocalCoordinate)>;
    using BoolGridFunctionLeaf = Dune::PDELab::LocalCallableToBoundaryConditionAdapter<BoolFunctionExpression>;
    
    // Where dirichlet-cond lampbda function
            //location diricleond for u
                BoolFunctionExpression diricletcond([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 1e-08 || x[1] > 0.99999999 ? 1 : 0.0);; });
            //TODO: HIER AUCH NOCH EINE FÜR P ERSTELLEN
            //location diricleond for u
                
        //dirichletcond u
            //dirichlet space u1
                BoolGridFunctionLeaf dirichlet_u1(diricletcond); //*
            //dirichlet space u2
                BoolGridFunctionLeaf dirichlet_u2(diricletcond);//*
            //zusammenpacken
            using CompositeConstraintsParameters_u1_is_u2 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(dirichlet_u1)>,std::decay_t<decltype(dirichlet_u2)>>; //*dirichlet_u1, *dirichlet_u2
                CompositeConstraintsParameters_u1_is_u2  is_diriclet_u(dirichlet_u1,dirichlet_u2); //*dirichlet_u1,*dirichlet_u2
            //dirichlet space p
            BoolGridFunctionLeaf dirichlet_p(diricletcond); //*diricletcond
            
            //GESAMMTER DIRICHLETBEREICH
            using BoundaryConditionType = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(is_diriclet_u)>,std::decay_t<decltype(dirichlet_p)>>; //*is_diriclet_u,*dirichlet_p
    
                BoundaryConditionType is_dirichlet(is_diriclet_u,dirichlet_p); //*is_diriclet_u,*dirichlet_p
        
            //actual dirichlet cond
                //0 auf Rand
                using FunctionExpression = std::function<RangeType(typename GV::template Codim<0>::Entity, typename GV::template Codim<0>::Entity::Geometry::LocalCoordinate)>;
    
                using GridFunctionLeaf = Dune::PDELab::LocalCallableToGridFunctionAdapter<GV, RangeType, 1, FunctionExpression>;
                
                FunctionExpression zeroOnBoundary([&](const auto& is, const auto& xl){ return 0.0; });
                    //u1
                    GridFunctionLeaf dBoundary_u1(gv, zeroOnBoundary); //*zeroOnBoundary
    
                //runter drücken auf Rand
                FunctionExpression pushdown([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 0.1 ? 0.0 : -0.1);; });
                    //u2
                    GridFunctionLeaf dBoundary_u2(gv,pushdown); //*pushdown
                //u1u2 together
                using dBOUNDARYu = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1)>,std::decay_t<decltype(dBoundary_u2)>>; //*dBoundary_u1,*dBoundary_u2
                    
                    dBOUNDARYu dBoundary_u1u2(dBoundary_u1, dBoundary_u2); //*dBoundary_u1, *dBoundary_u2
    
                    //p
                    GridFunctionLeaf dBoundary_p(gv, zeroOnBoundary); //zeroOnBoundary
    
                //DirichletBoundary Complete
                using BoundaryGridFunction = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1u2)>,std::decay_t<decltype(dBoundary_p)>>; //dBoundary_u1u2,dBoundary_p
        
                    BoundaryGridFunction BoundaryCondition(dBoundary_u1u2,dBoundary_p); //*dBoundary_u1u2,*dBoundary_p
        
    //Constrains
        //constrainscontainer
        using CONSTRAINS = typename CG1_GFS_POW2GFS_CG1_GFS::template ConstraintsContainer<RangeType>::Type;
        CONSTRAINS constrains;
        //clear
        constrains.clear();
        //Fill with constrains
        Dune::PDELab::constraints(is_dirichlet, combinationGFS, constrains); //*is_dirichlet
    
    
    //Gridoperator
        combinationGFS.update();
        //DOF-estimate
        int generic_dof_estimate =  6 * combinationGFS.maxLocalSize();
        int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    
        // MatrixBeackend erstellen
        //MARK: Nicht ischer ob rihtig, aus Tutorial1 übernommen
        using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
        MBE mbe((int)pow(1+2*degree,dim));
    
        //Make Gridoperator of r
        /*
        typedef Dune::PDELab::GridOperator<
        GFS,GFS,   ansatz and test space
        LOP,       local operator
        MBE,       matrix backend
        RF,RF,RF,  domain, range, jacobian field type
        CC,CC      constraints for ansatz and test space
        > GO;
        */
        using GRIDOPERATOR_r = Dune::PDELab::GridOperator<CG1_GFS_POW2GFS_CG1_GFS, CG1_GFS_POW2GFS_CG1_GFS, LOCAL_OPERATOR, MBE, DF, RangeType, RangeType, CONSTRAINS, CONSTRAINS>;
    
        GRIDOPERATOR_r go_r(combinationGFS, constrains, combinationGFS, constrains, lop_r, mbe); //*combinationGFS, *constrains, *combinationGFS, *constrains,*lop_r, *mbe
    
        std::cout << "gfs with " << combinationGFS.size() << " dofs generated  "<< std::endl;
        std::cout << "cc with " << constrains.size() << " dofs generated  "<< std::endl;

    //vector
    using V_R = Dune::PDELab::Backend::Vector<CG1_GFS_POW2GFS_CG1_GFS,DF>;
    V_R x_r(combinationGFS); //*combinationGFS

    x_r = 0.0;
    Dune::PDELab::interpolate(BoundaryCondition, combinationGFS, x_r);// *BoundaryCondition, *combinationGFS, *x_r
    



    //Solver
    using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_SuperLU; //Solver backend using SuperLU as a direct solver
    using SLP = Dune::PDELab::StationaryLinearProblemSolver<GRIDOPERATOR_r, LinearSolver, V_R>;
    //TODO: OBERES BEI INSTATIONARY NEWTON
    
    LinearSolver ls(false); //false bewirkt dass keine Massage geprintet wird
    double reduction = initree.get<double>("reduction", 1e-12);
    SLP slp (go_r, ls, x_r, reduction); //*go_r, *ls, *x_r


}
#endif //BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
