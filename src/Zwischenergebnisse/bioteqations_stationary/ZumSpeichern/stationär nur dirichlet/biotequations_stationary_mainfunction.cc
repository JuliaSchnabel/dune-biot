
#include "config.h"
#include "dune/common/parallel/mpihelper.hh"
#include "dune/alugrid/grid.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include <random>
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/codegen/vtkpredicate.hh"


#include"biotequations_stationary_driver_own.hh"
#include"bioteq_r_operator.hh"


int main(int argc, char** argv){
          //initialize MPI, finalize is done automatictly on exit
          Dune::MPIHelper::instance(argc, argv);
        try
        {

          if (argc != 2){
            std::cerr << "This program needs to be called with an ini file" << std::endl;
            return 1;
          }
            //read ini file
            Dune::ParameterTree initree;
            Dune::ParameterTreeParser::readINITree(argv[1], initree);
            //k for Pk
            const int degree = initree.get<int>("method.degree");
            
            // make Grid
            using Grid = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
            using GV = Grid::LeafGridView;
            IniGridFactory<Grid> factory(initree);
            std::shared_ptr<Grid> grid = factory.getGrid();
            GV gv = grid->leafGridView();

            
            //make finite element file and solve
            
            //FEM
            using RangeType = double;
            using DF = typename GV::ctype;
            if (degree==1) {
                using CG1_FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 1>;
                CG1_FEM cg1_fem(gv);
                driver(gv, cg1_fem,initree, degree);
            }
            else if (degree==2) {
                using CG1_FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 2>;
                CG1_FEM cg2_fem(gv);
                driver(gv, cg2_fem,initree, degree);
            }else{
                std::cerr << "degree only 1 or 2, hange in ini file" << std::endl;
            }
            
        
          }
          catch (Dune::Exception& e)
          {    std::cerr << "Dune reported error: " << e << std::endl;
            return 1;
          }
          catch (std::exception& e)
          {    std::cerr << "Unknown exception thrown!" << std::endl;
            return 1;
          }
               
}


