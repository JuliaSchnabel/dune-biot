#ifndef BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
#define BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH


#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/function/callableadapter.hh"



template<typename GV>
class DriverBlock
{  

  public:
  DriverBlock(GV& _gv, Dune::ParameterTree initree) :
      gv(_gv)
  {
    // Fem
    cg2_fem =  std::make_shared<CG2_FEM>(gv);
    
    // Gfs
    cg2_dirichlet_gfs_0_0 = std::make_shared<CG2_dirichlet_GFS>(gv, *cg2_fem);
    cg2_dirichlet_gfs_0_0->name("cg2_dirichlet_gfs_0_0");
    cg2_dirichlet_gfs_0_0_pow2gfs_0 = std::make_shared<CG2_dirichlet_GFS_POW2GFS>(*cg2_dirichlet_gfs_0_0);
    using namespace Dune::Indices;
    cg2_dirichlet_gfs_0_0_pow2gfs_0->child(_0).name("cg2_dirichlet_gfs_0_0_pow2gfs_0_0");
    cg2_dirichlet_gfs_0_0_pow2gfs_0->child(_1).name("cg2_dirichlet_gfs_0_0_pow2gfs_0_1");
    cg2_dirichlet_gfs_1 = std::make_shared<CG2_dirichlet_GFS>(gv, *cg2_fem);
    cg2_dirichlet_gfs_1->name("cg2_dirichlet_gfs_1");
    cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_ = std::make_shared<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS>(*cg2_dirichlet_gfs_0_0_pow2gfs_0, *cg2_dirichlet_gfs_1);
    cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_->update();
    
    // Localoperator
    lop_r = std::make_shared<LOP_R>(*cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_, initree);
    
    // Gridfunction
    functionExpression_0000 = std::make_shared<BoolFunctionExpression> ([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 1e-08 || x[1] > 0.99999999 ? 1 : 0.0);; });
    is_dirichlet_0000 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0000);
    is_dirichlet_0001 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0000);
    is_dirichlet_0000_is_dirichlet_0001 = std::make_shared<CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001>(*is_dirichlet_0000, *is_dirichlet_0001);
    is_dirichlet_0002 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0000);
    is_dirichlet = std::make_shared<BoundaryConditionType>(*is_dirichlet_0000_is_dirichlet_0001, *is_dirichlet_0002);
    functionExpression_0001 = std::make_shared<FunctionExpression> ([&](const auto& is, const auto& xl){ return 0.0; });
    interpolate_expression_0000 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0001);
    functionExpression_0002 = std::make_shared<FunctionExpression> ([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 0.1 ? 0.0 : -0.1);; });
    interpolate_expression_0001 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0002);
    interpolate_expression_0000_interpolate_expression_0001 = std::make_shared<CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001>(*interpolate_expression_0000, *interpolate_expression_0001);
    interpolate_expression_0002 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0001);
    interpolate_expression = std::make_shared<BoundaryGridFunction>(*interpolate_expression_0000_interpolate_expression_0001, *interpolate_expression_0002);
    
    // Constraints
    cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc = std::make_shared<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS_CC>();
    cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc->clear();
    Dune::PDELab::constraints(*is_dirichlet, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc);
    
    // Gridoperator
    cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_->update();
    int generic_dof_estimate =  6 * cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_->maxLocalSize();
    int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    mb = std::make_shared<MatrixBackend>(dofestimate);
    go_r = std::make_shared<GO_r>(*cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc, *lop_r, *mb);
    std::cout << "gfs with " << cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_->size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc->size() << " dofs generated  "<< std::endl;
    
    // Vector
    x_r = std::make_shared<V_R>(*cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_);
    *x_r = 0.0;
    Dune::PDELab::interpolate(*interpolate_expression, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_, *x_r);
    
    // Solver
    ls = std::make_shared<LinearSolver>(false);
    double reduction = initree.get<double>("reduction", 1e-12);
    slp = std::make_shared<SLP>(*go_r, *ls, *x_r, reduction);
    
  }
  

  public:
  GV gv;
  

  public:
  using VectorBackendCG2 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using DF = typename GV::ctype;
  

  public:
  using RangeType = double;
  

  public:
  using CG2_FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 2>;
  

  public:
  using DirichletConstraintsAssember = Dune::PDELab::ConformingDirichletConstraints;
  

  public:
  using CG2_dirichlet_GFS = Dune::PDELab::GridFunctionSpace<GV, CG2_FEM, DirichletConstraintsAssember, VectorBackendCG2>;
  

  public:
  using VectorBackendCG2_2 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using CG2_dirichlet_GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<CG2_dirichlet_GFS, 2, VectorBackendCG2_2, Dune::PDELab::LexicographicOrderingTag>;
  

  public:
  using VectorBackendCG2_2_CG2 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackendCG2_2_CG2, Dune::PDELab::LexicographicOrderingTag, CG2_dirichlet_GFS_POW2GFS, CG2_dirichlet_GFS>;
  

  public:
  using LOP_R = BiotEq_r_Operator<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS, CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS>;
  

  public:
  using CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS_CC = typename CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS::template ConstraintsContainer<RangeType>::Type;
  

  public:
  using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  

  public:
  using GO_r = Dune::PDELab::GridOperator<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS, CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS, LOP_R, MatrixBackend, DF, RangeType, RangeType, CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS_CC, CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS_CC>;
  

  public:
  using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_SuperLU;
  

  public:
  using V_R = Dune::PDELab::Backend::Vector<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS,DF>;
  

  public:
  using SLP = Dune::PDELab::StationaryLinearProblemSolver<GO_r, LinearSolver, V_R>;
  

  public:
  std::shared_ptr<SLP> slp;
  

  public:
  std::shared_ptr<GO_r> go_r;
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS> cg2_dirichlet_gfs_0_0;
  

  public:
  std::shared_ptr<CG2_FEM> cg2_fem;
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS_POW2GFS> cg2_dirichlet_gfs_0_0_pow2gfs_0;
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS> cg2_dirichlet_gfs_1;
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS> cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_;
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS> getGridFunctionsSpace(){
    return cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1_;
  }
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS_CC> cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc;
  

  public:
  std::shared_ptr<CG2_dirichlet_GFS_POW2GFS_CG2_dirichlet_GFS_CC> getConstraintsContainer(){
    return cg2_dirichlet_gfs_0_0_pow2gfs_0_cg2_dirichlet_gfs_1__cc;
  }
  

  public:
  using BoolFunctionExpression = std::function<bool(typename GV::Intersection, typename GV::Intersection::LocalCoordinate)>;
  

  public:
  using BoolGridFunctionLeaf = Dune::PDELab::LocalCallableToBoundaryConditionAdapter<BoolFunctionExpression>;
  

  public:
  std::shared_ptr<BoolGridFunctionLeaf> is_dirichlet_0000;
  

  public:
  std::shared_ptr<BoolFunctionExpression> functionExpression_0000;
  

  public:
  std::shared_ptr<BoolGridFunctionLeaf> is_dirichlet_0001;
  

  public:
  using CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(*is_dirichlet_0000)>,std::decay_t<decltype(*is_dirichlet_0001)>>;
  

  public:
  std::shared_ptr<CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001> is_dirichlet_0000_is_dirichlet_0001;
  

  public:
  std::shared_ptr<BoolGridFunctionLeaf> is_dirichlet_0002;
  

  public:
  using BoundaryConditionType = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(*is_dirichlet_0000_is_dirichlet_0001)>,std::decay_t<decltype(*is_dirichlet_0002)>>;
  

  public:
  std::shared_ptr<BoundaryConditionType> is_dirichlet;
  

  public:
  std::shared_ptr<LOP_R> lop_r;
  

  public:
  std::shared_ptr<LOP_R> getLocalOperator(){
    return lop_r;
  }
  

  public:
  std::shared_ptr<MatrixBackend> mb;
  

  public:
  std::shared_ptr<GO_r> getGridOperator(){
    return go_r;
  }
  

  public:
  std::shared_ptr<LinearSolver> ls;
  

  public:
  std::shared_ptr<V_R> x_r;
  

  public:
  std::shared_ptr<V_R> getCoefficient(){
    return x_r;
  }
  

  public:
  using FunctionExpression = std::function<RangeType(typename GV::template Codim<0>::Entity, typename GV::template Codim<0>::Entity::Geometry::LocalCoordinate)>;
  

  public:
  using GridFunctionLeaf = Dune::PDELab::LocalCallableToGridFunctionAdapter<GV, RangeType, 1, FunctionExpression>;
  

  public:
  std::shared_ptr<GridFunctionLeaf> interpolate_expression_0000;
  

  public:
  std::shared_ptr<FunctionExpression> functionExpression_0001;
  

  public:
  std::shared_ptr<GridFunctionLeaf> interpolate_expression_0001;
  

  public:
  std::shared_ptr<FunctionExpression> functionExpression_0002;
  

  public:
  using CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001 = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(*interpolate_expression_0000)>,std::decay_t<decltype(*interpolate_expression_0001)>>;
  

  public:
  std::shared_ptr<CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001> interpolate_expression_0000_interpolate_expression_0001;
  

  public:
  std::shared_ptr<GridFunctionLeaf> interpolate_expression_0002;
  

  public:
  using BoundaryGridFunction = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(*interpolate_expression_0000_interpolate_expression_0001)>,std::decay_t<decltype(*interpolate_expression_0002)>>;
  

  public:
  std::shared_ptr<BoundaryGridFunction> interpolate_expression;
  

  public:
  std::shared_ptr<SLP> getSolver(){
    return slp;
  }
};




#endif //BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
