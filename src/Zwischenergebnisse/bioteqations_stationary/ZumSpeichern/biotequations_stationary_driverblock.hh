#ifndef BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
#define BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH


#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"



template<typename GV>
class DriverBlock
{  

  public:
  DriverBlock(GV& _gv, Dune::ParameterTree initree) :
      gv(_gv)
  {
    // Fem
    //CG1_FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 1>;
    cg1_fem =  std::make_shared<CG1_FEM>(gv); //----
    
    // Gfs
     // using VectorBackendCG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
     // using NoConstraintsAssembler = Dune::PDELab::NoConstraints;
     // using CG1_GFS = Dune::PDELab::GridFunctionSpace<GV, CG1_FEM, NoConstraintsAssembler, VectorBackendCG1>;
    cg1_gfs_0_0 = std::make_shared<CG1_GFS>(gv, *cg1_fem); //--------gfsfür u
    cg1_gfs_0_0->name("cg1_gfs_0_0"); //----------
      //std::shared_ptr<CG1_GFS_POW2GFS> cg1_gfs_0_0_pow2gfs_0;
      //  using CG1_GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<CG1_GFS, 2, VectorBackendCG1_2, Dune::PDELab::LexicographicOrderingTag>;
    cg1_gfs_0_0_pow2gfs_0 = std::make_shared<CG1_GFS_POW2GFS>(*cg1_gfs_0_0); //gfs für u1u2 ------
    using namespace Dune::Indices;
    cg1_gfs_0_0_pow2gfs_0->child(_0).name("cg1_gfs_0_0_pow2gfs_0_0"); //gfs u1
    cg1_gfs_0_0_pow2gfs_0->child(_1).name("cg1_gfs_0_0_pow2gfs_0_1"); //gfs u2
    
      //GFS für p
    cg1_gfs_1 = std::make_shared<CG1_GFS>(gv, *cg1_fem);
    cg1_gfs_1->name("cg1_gfs_1"); //----für p
      //zusammfügen
    cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_ = std::make_shared<CG1_GFS_POW2GFS_CG1_GFS>(*cg1_gfs_0_0_pow2gfs_0, *cg1_gfs_1); //---------
    cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_->update();//------
    
    // Localoperator
    lop_r = std::make_shared<LOP_R>(*cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_, initree);
      //----
    
    //____________________________________________________________________________________
      // Gridfunction
        //wo dirichletcond u
      functionExpression_0000 = std::make_shared<BoolFunctionExpression> ([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 1e-08 || x[1] > 0.99999999 ? 1 : 0.0);; });
        //für u1u2
      is_dirichlet_0000 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0000);//----
      is_dirichlet_0001 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0000);//_____
      is_dirichlet_0000_is_dirichlet_0001 = std::make_shared<CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001>(*is_dirichlet_0000, *is_dirichlet_0001); //______________
        //für p
      is_dirichlet_0002 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0000);
      is_dirichlet = std::make_shared<BoundaryConditionType>(*is_dirichlet_0000_is_dirichlet_0001, *is_dirichlet_0002);//_____
        //dirichlet festlegen
      functionExpression_0001 = std::make_shared<FunctionExpression> ([&](const auto& is, const auto& xl){ return 0.0; });
            //u1
      interpolate_expression_0000 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0001);//______
      functionExpression_0002 = std::make_shared<FunctionExpression> ([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 0.1 ? 0.0 : -0.1);; });//____
            //u2
      interpolate_expression_0001 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0002);//______
            //zusammen u1u2
      interpolate_expression_0000_interpolate_expression_0001 = std::make_shared<CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001>(*interpolate_expression_0000, *interpolate_expression_0001); //________
            //p
      interpolate_expression_0002 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0001);
            //zusammen gesammt
      interpolate_expression = std::make_shared<BoundaryGridFunction>(*interpolate_expression_0000_interpolate_expression_0001, *interpolate_expression_0002);
      
      
      // Constraints
      cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc = std::make_shared<CG2_dirichlet_GFS_POW2GFS_CG1_GFS_CC>();
      cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc->clear();
      Dune::PDELab::constraints(*is_dirichlet, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc);
      
      // Gridoperator
      cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_->update();
      int generic_dof_estimate =  6 * cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_->maxLocalSize();
      int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
      mb = std::make_shared<MatrixBackend>(dofestimate);
      go_r = std::make_shared<GO_r>(*cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc, *lop_r, *mb);
      std::cout << "gfs with " << cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_->size() << " dofs generated  "<< std::endl;
      std::cout << "cc with " << cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc->size() << " dofs generated  "<< std::endl;
      
      // Vector
      x_r = std::make_shared<V_R>(*cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_);
      *x_r = 0.0;
      Dune::PDELab::interpolate(*interpolate_expression, *cg2_dirichlet_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *x_r);
      
      // Solver
      ls = std::make_shared<LinearSolver>(false);
      double reduction = initree.get<double>("reduction", 1e-12);
      slp = std::make_shared<SLP>(*go_r, *ls, *x_r, reduction);
     //_____________________________________________________________________________________________________
    // Constraints
    cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc = std::make_shared<CG1_GFS_POW2GFS_CG1_GFS_CC>();
    cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc->clear();
    Dune::PDELab::constraints(*cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc);
    
    // Gridoperator
    cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_->update();
    int generic_dof_estimate =  6 * cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_->maxLocalSize();
    int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    mb = std::make_shared<MatrixBackend>(dofestimate);
    go_r = std::make_shared<GO_r>(*cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc, *cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_, *cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc, *lop_r, *mb);
    std::cout << "gfs with " << cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_->size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc->size() << " dofs generated  "<< std::endl;
    
    // Vector
    x_r = std::make_shared<V_R>(*cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_);
    *x_r = 0.0;
    
    // Solver
    ls = std::make_shared<LinearSolver>(false);
    double reduction = initree.get<double>("reduction", 1e-12);
    slp = std::make_shared<SLP>(*go_r, *ls, *x_r, reduction);
    
  }
  

  public:
  GV gv;
  

  public:
  using VectorBackendCG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using DF = typename GV::ctype;
  

  public:
  using RangeType = double;
  

  public:
  using CG1_FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 1>;
  

  public:
  using NoConstraintsAssembler = Dune::PDELab::NoConstraints;
  

  public:
  using CG1_GFS = Dune::PDELab::GridFunctionSpace<GV, CG1_FEM, NoConstraintsAssembler, VectorBackendCG1>;
  

  public:
  using VectorBackendCG1_2 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using CG1_GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<CG1_GFS, 2, VectorBackendCG1_2, Dune::PDELab::LexicographicOrderingTag>;
  

  public:
  using VectorBackendCG1_2_CG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using CG1_GFS_POW2GFS_CG1_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackendCG1_2_CG1, Dune::PDELab::LexicographicOrderingTag, CG1_GFS_POW2GFS, CG1_GFS>;
  

  public:
  using LOP_R = BiotEq_r_Operator<CG1_GFS_POW2GFS_CG1_GFS, CG1_GFS_POW2GFS_CG1_GFS>;
  

  public:
  using CG1_GFS_POW2GFS_CG1_GFS_CC = typename CG1_GFS_POW2GFS_CG1_GFS::template ConstraintsContainer<RangeType>::Type;
  

  public:
  using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  

  public:
  using GO_r = Dune::PDELab::GridOperator<CG1_GFS_POW2GFS_CG1_GFS, CG1_GFS_POW2GFS_CG1_GFS, LOP_R, MatrixBackend, DF, RangeType, RangeType, CG1_GFS_POW2GFS_CG1_GFS_CC, CG1_GFS_POW2GFS_CG1_GFS_CC>;
  

  public:
  using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_SuperLU;
  

  public:
  using V_R = Dune::PDELab::Backend::Vector<CG1_GFS_POW2GFS_CG1_GFS,DF>;
  

  public:
  using SLP = Dune::PDELab::StationaryLinearProblemSolver<GO_r, LinearSolver, V_R>;
  

  public:
  std::shared_ptr<SLP> slp;
  

  public:
  std::shared_ptr<GO_r> go_r;
  

  public:
  std::shared_ptr<CG1_GFS> cg1_gfs_0_0;
  

  public:
  std::shared_ptr<CG1_FEM> cg1_fem;
  

  public:
  std::shared_ptr<CG1_GFS_POW2GFS> cg1_gfs_0_0_pow2gfs_0;
  

  public:
  std::shared_ptr<CG1_GFS> cg1_gfs_1;
  

  public:
  std::shared_ptr<CG1_GFS_POW2GFS_CG1_GFS> cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_;
  

  public:
  std::shared_ptr<CG1_GFS_POW2GFS_CG1_GFS> getGridFunctionsSpace(){
    return cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1_;
  }
  

  public:
  std::shared_ptr<CG1_GFS_POW2GFS_CG1_GFS_CC> cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc;
  

  public:
  std::shared_ptr<CG1_GFS_POW2GFS_CG1_GFS_CC> getConstraintsContainer(){
    return cg1_gfs_0_0_pow2gfs_0_cg1_gfs_1__cc;
  }
  

  public:
  std::shared_ptr<LOP_R> lop_r;
  

  public:
  std::shared_ptr<LOP_R> getLocalOperator(){
    return lop_r;
  }
  

  public:
  std::shared_ptr<MatrixBackend> mb;
  

  public:
  std::shared_ptr<GO_r> getGridOperator(){
    return go_r;
  }
  

  public:
  std::shared_ptr<LinearSolver> ls;
  

  public:
  std::shared_ptr<V_R> x_r;
  

  public:
  std::shared_ptr<V_R> getCoefficient(){
    return x_r;
  }
  

  public:
  std::shared_ptr<SLP> getSolver(){
    return slp;
  }
};




#endif //BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
