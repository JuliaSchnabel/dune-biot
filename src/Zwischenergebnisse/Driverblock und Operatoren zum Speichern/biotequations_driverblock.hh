#ifndef BIOTEQUATIONS_DRIVERBLOCK_HH
#define BIOTEQUATIONS_DRIVERBLOCK_HH


#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/gridoperator/onestep.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "biotequations_massOperator_file.hh"
#include "dune/pdelab/newton/newton.hh"



template<typename GV>
class DriverBlock
{  

  public:
  DriverBlock(GV& _gv, Dune::ParameterTree initree) :
      gv(_gv)
  {
    // Fem
    cg1_fem =  std::make_shared<CG1_FEM>(gv);
    
    // Gfs
    cg1_dirichlet_gfs_0_0 = std::make_shared<CG1_dirichlet_GFS>(gv, *cg1_fem);
    cg1_dirichlet_gfs_0_0->name("cg1_dirichlet_gfs_0_0");
    cg1_dirichlet_gfs_0_0_pow2gfs_0 = std::make_shared<CG1_dirichlet_GFS_POW2GFS>(*cg1_dirichlet_gfs_0_0);
    using namespace Dune::Indices;
    cg1_dirichlet_gfs_0_0_pow2gfs_0->child(_0).name("cg1_dirichlet_gfs_0_0_pow2gfs_0_0");
    cg1_dirichlet_gfs_0_0_pow2gfs_0->child(_1).name("cg1_dirichlet_gfs_0_0_pow2gfs_0_1");
    cg1_dirichlet_gfs_1 = std::make_shared<CG1_dirichlet_GFS>(gv, *cg1_fem);
    cg1_dirichlet_gfs_1->name("cg1_dirichlet_gfs_1");
    cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_ = std::make_shared<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS>(*cg1_dirichlet_gfs_0_0_pow2gfs_0, *cg1_dirichlet_gfs_1);
    cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_->update();
    
    // Localoperator
    lop_r = std::make_shared<LOP_R>(*cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, initree);
    lop_mass = std::make_shared<LOP_MASS>(*cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, initree);
    
    // Gridfunction
    functionExpression_0000 = std::make_shared<FunctionExpression> ([&](const auto& is, const auto& xl){ return 0.0; });
    interpolate_expression_0000 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0000, *lop_r);
    interpolate_expression_0001 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0000, *lop_r);
    interpolate_expression_0000_interpolate_expression_0001 = std::make_shared<CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001>(*interpolate_expression_0000, *interpolate_expression_0001);
    functionExpression_0001 = std::make_shared<FunctionExpression> ([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 0.1 ? 0.0 : -0.1);; });
    interpolate_expression_0002 = std::make_shared<GridFunctionLeaf>(gv, *functionExpression_0001, *lop_r);
    interpolate_expression = std::make_shared<BoundaryGridFunction>(*interpolate_expression_0000_interpolate_expression_0001, *interpolate_expression_0002);
    functionExpression_0002 = std::make_shared<BoolFunctionExpression> ([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[1] < 1e-08 || x[1] > 0.99999999 ? 1 : 0.0);; });
    is_dirichlet_0000 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0002);
    is_dirichlet_0001 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0002);
    is_dirichlet_0000_is_dirichlet_0001 = std::make_shared<CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001>(*is_dirichlet_0000, *is_dirichlet_0001);
    is_dirichlet_0002 = std::make_shared<BoolGridFunctionLeaf>(*functionExpression_0002);
    is_dirichlet = std::make_shared<BoundaryConditionType>(*is_dirichlet_0000_is_dirichlet_0001, *is_dirichlet_0002);
    
    // Constraints
    cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc = std::make_shared<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC>();
    cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc->clear();
    Dune::PDELab::constraints(*is_dirichlet, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc);
    
    // Gridoperator
    cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_->update();
    int generic_dof_estimate =  6 * cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_->maxLocalSize();
    int dofestimate = initree.get<int>("istl.number_of_nnz", generic_dof_estimate);
    mb = std::make_shared<MatrixBackend>(dofestimate);
    go_r = std::make_shared<GO_r>(*cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc, *lop_r, *mb);
    std::cout << "gfs with " << cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_->size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc->size() << " dofs generated  "<< std::endl;
    go_mass = std::make_shared<GO_mass>(*cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc, *lop_mass, *mb);
    std::cout << "gfs with " << cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_->size() << " dofs generated  "<< std::endl;
    std::cout << "cc with " << cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc->size() << " dofs generated  "<< std::endl;
    igo = std::make_shared<IGO>(*go_r, *go_mass);
    
    // Vector
    x_r = std::make_shared<V_R>(*cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_);
    *x_r = 0.0;
    Dune::PDELab::interpolate(*interpolate_expression, *cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_, *x_r);
    
    // Solver
    ls = std::make_shared<LinearSolver>(false);
    snp = std::make_shared<SNP>(*igo, *x_r, *ls);
    
    // Instat
    tsm = std::make_shared<TSM>();
    osm = std::make_shared<OSM>(*tsm, *igo, *snp);
    
  }
  

  public:
  GV gv;
  

  public:
  using VectorBackendCG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using DF = typename GV::ctype;
  

  public:
  using RangeType = double;
  

  public:
  using CG1_FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 1>;
  

  public:
  using DirichletConstraintsAssember = Dune::PDELab::ConformingDirichletConstraints;
  

  public:
  using CG1_dirichlet_GFS = Dune::PDELab::GridFunctionSpace<GV, CG1_FEM, DirichletConstraintsAssember, VectorBackendCG1>;
  

  public:
  using VectorBackendCG1_2 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using CG1_dirichlet_GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<CG1_dirichlet_GFS, 2, VectorBackendCG1_2, Dune::PDELab::LexicographicOrderingTag>;
  

  public:
  using VectorBackendCG1_2_CG1 = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
  

  public:
  using CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackendCG1_2_CG1, Dune::PDELab::LexicographicOrderingTag, CG1_dirichlet_GFS_POW2GFS, CG1_dirichlet_GFS>;
  

  public:
  using LOP_R = BiotEq_r_Operator<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS>;
  

  public:
  std::shared_ptr<LOP_R> lop_r;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS> cg1_dirichlet_gfs_0_0;
  

  public:
  std::shared_ptr<CG1_FEM> cg1_fem;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS_POW2GFS> cg1_dirichlet_gfs_0_0_pow2gfs_0;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS> cg1_dirichlet_gfs_1;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS> cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS> getGridFunctionsSpace(){
    return cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1_;
  }
  

  public:
  std::shared_ptr<LOP_R> getLocalOperator(){
    return lop_r;
  }
  

  public:
  using V_R = Dune::PDELab::Backend::Vector<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS,DF>;
  

  public:
  std::shared_ptr<V_R> x_r;
  

  public:
  using FunctionExpression = std::function<RangeType(typename GV::template Codim<0>::Entity, typename GV::template Codim<0>::Entity::Geometry::LocalCoordinate)>;
  

  public:
  using GridFunctionLeaf = Dune::PDELab::LocalCallableToInstationaryGridFunctionAdapter<GV, RangeType, 1, FunctionExpression, LOP_R>;
  

  public:
  std::shared_ptr<GridFunctionLeaf> interpolate_expression_0000;
  

  public:
  std::shared_ptr<FunctionExpression> functionExpression_0000;
  

  public:
  std::shared_ptr<GridFunctionLeaf> interpolate_expression_0001;
  

  public:
  using CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001 = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(*interpolate_expression_0000)>,std::decay_t<decltype(*interpolate_expression_0001)>>;
  

  public:
  std::shared_ptr<CompositeGridFunction_interpolate_expression_0000_interpolate_expression_0001> interpolate_expression_0000_interpolate_expression_0001;
  

  public:
  std::shared_ptr<GridFunctionLeaf> interpolate_expression_0002;
  

  public:
  std::shared_ptr<FunctionExpression> functionExpression_0001;
  

  public:
  using BoundaryGridFunction = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(*interpolate_expression_0000_interpolate_expression_0001)>,std::decay_t<decltype(*interpolate_expression_0002)>>;
  

  public:
  std::shared_ptr<BoundaryGridFunction> interpolate_expression;
  

  public:
  std::shared_ptr<V_R> getCoefficient(){
    return x_r;
  }
  

  public:
  using BoolFunctionExpression = std::function<bool(typename GV::Intersection, typename GV::Intersection::LocalCoordinate)>;
  

  public:
  using BoolGridFunctionLeaf = Dune::PDELab::LocalCallableToBoundaryConditionAdapter<BoolFunctionExpression>;
  

  public:
  std::shared_ptr<BoolGridFunctionLeaf> is_dirichlet_0000;
  

  public:
  std::shared_ptr<BoolFunctionExpression> functionExpression_0002;
  

  public:
  std::shared_ptr<BoolGridFunctionLeaf> is_dirichlet_0001;
  

  public:
  using CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(*is_dirichlet_0000)>,std::decay_t<decltype(*is_dirichlet_0001)>>;
  

  public:
  std::shared_ptr<CompositeConstraintsParameters_is_dirichlet_0000_is_dirichlet_0001> is_dirichlet_0000_is_dirichlet_0001;
  

  public:
  std::shared_ptr<BoolGridFunctionLeaf> is_dirichlet_0002;
  

  public:
  using BoundaryConditionType = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(*is_dirichlet_0000_is_dirichlet_0001)>,std::decay_t<decltype(*is_dirichlet_0002)>>;
  

  public:
  std::shared_ptr<BoundaryConditionType> is_dirichlet;
  

  public:
  std::shared_ptr<BoundaryConditionType> getBoundaryConditionType(){
    return is_dirichlet;
  }
  

  public:
  using CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC = typename CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS::template ConstraintsContainer<RangeType>::Type;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC> cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc;
  

  public:
  std::shared_ptr<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC> getConstraintsContainer(){
    return cg1_dirichlet_gfs_0_0_pow2gfs_0_cg1_dirichlet_gfs_1__cc;
  }
  

  public:
  using MatrixBackend = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  

  public:
  using GO_r = Dune::PDELab::GridOperator<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS, LOP_R, MatrixBackend, DF, RangeType, RangeType, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC>;
  

  public:
  using LOP_MASS = massOperator<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS>;
  

  public:
  using GO_mass = Dune::PDELab::GridOperator<CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS, LOP_MASS, MatrixBackend, DF, RangeType, RangeType, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC, CG1_dirichlet_GFS_POW2GFS_CG1_dirichlet_GFS_CC>;
  

  public:
  using IGO = Dune::PDELab::OneStepGridOperator<GO_r,GO_mass>;
  

  public:
  using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_SuperLU;
  

  public:
  using SNP = Dune::PDELab::Newton<IGO, LinearSolver, V_R>;
  

  public:
  using OSM = Dune::PDELab::OneStepMethod<RangeType, IGO, SNP, V_R, V_R>;
  

  public:
  std::shared_ptr<OSM> osm;
  

  public:
  using TSM = Dune::PDELab::Alexander2Parameter<RangeType>;
  

  public:
  std::shared_ptr<TSM> tsm;
  

  public:
  std::shared_ptr<IGO> igo;
  

  public:
  std::shared_ptr<GO_r> go_r;
  

  public:
  std::shared_ptr<MatrixBackend> mb;
  

  public:
  std::shared_ptr<GO_r> getGridOperator(){
    return go_r;
  }
  

  public:
  std::shared_ptr<GO_mass> go_mass;
  

  public:
  std::shared_ptr<LOP_MASS> lop_mass;
  

  public:
  std::shared_ptr<LOP_MASS> getLocalMassOperator(){
    return lop_mass;
  }
  

  public:
  std::shared_ptr<GO_mass> getMassGridOperator(){
    return go_mass;
  }
  

  public:
  std::shared_ptr<SNP> snp;
  

  public:
  std::shared_ptr<LinearSolver> ls;
  

  public:
  std::shared_ptr<SNP> getSolver(){
    return snp;
  }
  

  public:
  using EOSM = Dune::PDELab::ExplicitOneStepMethod<RangeType, IGO, LinearSolver, V_R>;
  

  public:
  std::shared_ptr<OSM> getOneStepMethod(){
    return osm;
  }
  

  public:
  std::shared_ptr<BoundaryGridFunction> getBoundaryGridFunction(){
    return interpolate_expression;
  }
  

  public:
  void setTime(double t){
    lop_r->setTime(t);
  }
};




#endif //BIOTEQUATIONS_DRIVERBLOCK_HH
