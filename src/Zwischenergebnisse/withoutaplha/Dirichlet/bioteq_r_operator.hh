#ifndef BIOTEQ_R_OPERATOR_HH
#define BIOTEQ_R_OPERATOR_HH


#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/common/parametertree.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/codegen/localbasiscache.hh"
#include "dune/typetree/childextraction.hh"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"


template<typename GFSU, typename GFSV>
class BiotEq_r_Operator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern
{  

  public:
  BiotEq_r_Operator(const GFSU& gfsu, const GFSV& gfsv, const Dune::ParameterTree& iniParams) :
      _iniParams(iniParams)
  {
    fillQuadraturePointsCache(gfsu.gridView().template begin<0>()->geometry(), 0, qp_dim2_order0);
    fillQuadratureWeightsCache(gfsu.gridView().template begin<0>()->geometry(), 0, qw_dim2_order0);
  }
  

  public:
  const Dune::ParameterTree& _iniParams;
  

  public:
  enum { doPatternVolume = true };
  

  public:
  enum { isLinear = true };
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Vector> qp_dim2_order0;
  

  public:
  using GFSV_0 = Dune::TypeTree::Child<GFSV,0>;
  

  public:
  using GFSV_0_0 = Dune::TypeTree::Child<GFSV_0,0>;
  

  public:
  using P1_LocalBasis = typename GFSV_0_0::Traits::FiniteElementMap::Traits::FiniteElementType::Traits::LocalBasisType;
  

  public:
  LocalBasisCacheWithoutReferences<P1_LocalBasis> cache_CG1;
  

  public:
  using GFSV_0_1 = Dune::TypeTree::Child<GFSV_0,1>;
  

  public:
  using GFSV_1 = Dune::TypeTree::Child<GFSV,1>;
  

  public:
  mutable std::vector<typename Dune::QuadraturePoint<double, 2>::Field> qw_dim2_order0;
  

  public:
  enum { doAlphaVolume = true };
  

  public:
  template<typename X, typename EG, typename R, typename LFSV, typename LFSU>
  void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 0);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_0 = child(lfsv_0, _0);
    auto lfsv_0_0_size = lfsv_0_0.size();
    auto lfsv_0_1 = child(lfsv_0, _1);
    auto lfsv_0_1_size = lfsv_0_1.size();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    double acc_lfsv_0_0_0_trialgrad_index;
    double acc_lfsv_0_0_0_trialgrad_index_0;
    double acc_lfsv_0_0_0_trialgrad_index_1;
    double detjac;
    double gradu_0[2];
    double gradu_1[2];
    double gradu_2[2];
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P1_LocalBasis>::JacobianReturnType js_CG1;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      detjac = cell_geo.integrationElement(qp_dim2_order0[q]);
      js_CG1 = cache_CG1.evaluateJacobian(qp_dim2_order0[q], lfsv_0_0.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order0[q]);
      for (int idim0 = 0; idim0 <= 1; ++idim0)
      {
        acc_lfsv_0_0_0_trialgrad_index_1 = 0.0;
        acc_lfsv_0_0_0_trialgrad_index_0 = 0.0;
        acc_lfsv_0_0_0_trialgrad_index = 0.0;
        for (int lfsv_0_0_0_trialgrad_index = 0; lfsv_0_0_0_trialgrad_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_0_trialgrad_index)
        {
          acc_lfsv_0_0_0_trialgrad_index_1 = x(lfsv_1, lfsv_0_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_0_trialgrad_index_1;
          acc_lfsv_0_0_0_trialgrad_index_0 = x(lfsv_0_1, lfsv_0_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_0_trialgrad_index_0;
          acc_lfsv_0_0_0_trialgrad_index = x(lfsv_0_0, lfsv_0_0_0_trialgrad_index) * ((js_CG1[lfsv_0_0_0_trialgrad_index])[0])[idim0] + acc_lfsv_0_0_0_trialgrad_index;
        }
        gradu_2[idim0] = acc_lfsv_0_0_0_trialgrad_index_1;
        gradu_1[idim0] = acc_lfsv_0_0_0_trialgrad_index_0;
        gradu_0[idim0] = acc_lfsv_0_0_0_trialgrad_index;
      }
      for (int lfsv_0_0_0_index = 0; lfsv_0_0_0_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_0_index)
      {
        r.accumulate(lfsv_1, lfsv_0_0_0_index, (((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_2[0] + (jit[0])[1] * gradu_2[1]) + ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_2[0] + (jit[1])[1] * gradu_2[1])) * qw_dim2_order0[q] * detjac);
        r.accumulate(lfsv_0_1, lfsv_0_0_0_index, (224.93813695072427 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]) + 225.00562514062852 * (0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1] + (jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1] + (jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]))) * qw_dim2_order0[q] * detjac);
        r.accumulate(lfsv_0_0, lfsv_0_0_0_index, (224.93813695072427 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[1])[0] * gradu_1[0] + (jit[1])[1] * gradu_1[1]) + 225.00562514062852 * (0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1] + (jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1] + (jit[0])[0] * gradu_0[0] + (jit[0])[1] * gradu_0[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * gradu_1[0] + (jit[0])[1] * gradu_1[1] + (jit[1])[0] * gradu_0[0] + (jit[1])[1] * gradu_0[1]))) * qw_dim2_order0[q] * detjac);
      }
    }
  }
  

  public:
  template<typename X, typename J, typename EG, typename LFSV, typename LFSU>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = quadratureRule(cell_geo, 0);
    auto quadrature_size = quadrature_rule.size();
    using namespace Dune::Indices;
    auto lfsv_0 = child(lfsv, _0);
    auto lfsv_0_0 = child(lfsv_0, _0);
    auto lfsv_0_0_size = lfsv_0_0.size();
    auto lfsv_0_1 = child(lfsv_0, _1);
    auto lfsv_0_1_size = lfsv_0_1.size();
    auto lfsv_1 = child(lfsv, _1);
    auto lfsv_1_size = lfsv_1.size();
    double detjac;
    Dune::FieldMatrix<double, 2, 2> jit(0.0);
    typename LocalBasisCacheWithoutReferences<P1_LocalBasis>::JacobianReturnType js_CG1;
  
    for (int q = 0; q <= -1 + quadrature_size; ++q)
    {
      detjac = cell_geo.integrationElement(qp_dim2_order0[q]);
      js_CG1 = cache_CG1.evaluateJacobian(qp_dim2_order0[q], lfsv_0_0.finiteElement().localBasis());
      jit = cell_geo.jacobianInverseTransposed(qp_dim2_order0[q]);
      for (int lfsv_0_0_1_index = 0; lfsv_0_0_1_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_1_index)
        for (int lfsv_0_0_0_index = 0; lfsv_0_0_0_index <= -1 + lfsv_0_0_size; ++lfsv_0_0_0_index)
        {
          jac.accumulate(lfsv_1, lfsv_0_0_0_index, lfsv_1, lfsv_0_0_1_index, (((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1])) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_1, lfsv_0_0_0_index, lfsv_0_1, lfsv_0_0_1_index, (224.93813695072427 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1] + (jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_1, lfsv_0_0_0_index, lfsv_0_0, lfsv_0_0_1_index, (224.93813695072427 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_0, lfsv_0_0_0_index, lfsv_0_1, lfsv_0_0_1_index, (224.93813695072427 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
          jac.accumulate(lfsv_0_0, lfsv_0_0_0_index, lfsv_0_0, lfsv_0_0_1_index, (224.93813695072427 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 225.00562514062852 * (0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1] + (jit[0])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1] + (jit[0])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[0])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]) + 0.25 * ((jit[1])[0] * ((js_CG1[lfsv_0_0_0_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_0_index])[0])[1]) * ((jit[1])[0] * ((js_CG1[lfsv_0_0_1_index])[0])[0] + (jit[1])[1] * ((js_CG1[lfsv_0_0_1_index])[0])[1]))) * qw_dim2_order0[q] * detjac);
        }
    }
  }
};


#pragma GCC diagnostic pop

#endif //BIOTEQ_R_OPERATOR_HH
