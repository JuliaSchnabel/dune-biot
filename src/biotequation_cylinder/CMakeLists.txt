dune_add_generated_executable(TARGET biot_3D_cylinder_Operators
                              UFLFILE biotequations_3D.ufl 
                              INIFILE biotequations_3D.ini)

add_subdirectory(own_cylinder)

dune_symlink_to_source_files(FILES biotequations_3D.ini)
