
#include "config.h"
#include <dune/grid/uggrid.hh>
#include "dune/common/parallel/mpihelper.hh"
#include "dune/alugrid/grid.hh"
#include "dune/testtools/gridconstruction.hh"
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include <random>
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/codegen/vtkpredicate.hh"
#include<dune/common/timer.hh>


#include"biotequations_driver.hh"
#include"bioteq_r_operator.hh"
#include"biot_3D_di_massOperator_file.hh"



int main(int argc, char** argv){
          //initialize MPI, finalize is done automatictly on exit
          Dune::MPIHelper::instance(argc, argv);
        try
        {   Dune::Timer timer;

          if (argc != 2){
            std::cerr << "This program needs to be called with an ini file" << std::endl;
            return 1;
          }
            //read ini file
            Dune::ParameterTree initree;
            Dune::ParameterTreeParser::readINITree(argv[1], initree);
            //k for Pk
            const int degree = initree.get<int>("method.degree");
            std::string grid_file = initree.get<std::string>("grid.filename");
            
            
            // make Grid
            typedef Dune::UGGrid<3> GridType;
            Dune::GridFactory<GridType> factory;
            
            Dune::GmshReader<GridType>::read(factory,grid_file,true,true);
            std::shared_ptr<GridType> grid(factory.createGrid());
            
//            using Grid = Dune::ALUGrid<3, 3, Dune::simplex, Dune::conforming>;
            using GV = GridType::LeafGridView;
//            IniGridFactory<Grid> factory(initree);
//            std::shared_ptr<Grid> pgrid = factory.getGrid();
//            Grid & grid = *pgrid;
            GV gv = grid->leafGridView();

            
//            // make Grid
//            using Grid = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
//            using GV = Grid::LeafGridView;
//            IniGridFactory<Grid> factory(initree);
//            std::shared_ptr<Grid> grid = factory.getGrid();
//            GV gv = grid->leafGridView();
            
            
            
            //make finite element file and solve
            
            //FEM
            using RangeType = double;
            using DF = typename GV::ctype;
            if (degree==1) {
                using FEM = Dune::PDELab::PkLocalFiniteElementMap<GV, DF, RangeType, 1>;
                FEM fem(gv);
                driver(grid, gv, fem,initree, degree);
            }
            else{
                std::cerr << "degree only 1 , change in ini file" << std::endl;
            }
            
            std::cout << "Time to complete: " << timer.elapsed()
                      << " seconds" << std::endl;
            
        
          }
          catch (Dune::Exception& e)
          {    std::cerr << "Dune reported error: " << e << std::endl;
            return 1;
          }
          catch (std::exception& e)
          {    std::cerr << "Unknown exception thrown!" << std::endl;
            return 1;
          }
               
}



