#!/usr/bin/env python

# Import modules:
import gmsh
import sys
import math

#wirte txt file
#"x" - Create - will create a file, returns an error if the file exist
#"a" - Append - will create a file if the specified file does not exist
#"w" - Write - will create a file if the specified file does not exist
text = open("Dirichletcond.txt", "w")

# Initialize gmsh:
gmsh.initialize()

#Values for mesh-------------------------

#Origin for Base of the zylinder
origin=[0,0]

#adjust size of zylinder
diameter = 20


#adjust Count of pores (Pores = polygon_corners* pores_per_side)
polygon_corners = 67
pores_per_side = 10
diameter_pores = 0.05
diamamter_pores_around = 0.15
print('polygon_corners = ', polygon_corners)
print('pores per side = ', pores_per_side)
print('Pores on the Sides = ', polygon_corners * pores_per_side)
accuracy = 1e-08

text.write('Corners:')
text.write(str(polygon_corners))
text.write('\n')
text.write('Pores_per_side:')
text.write(str(pores_per_side))
text.write('\n')
text.write('----------------------')
text.write('\n')

#with volume or not (1 == volume)
with_volume = 1

#with squares around
with_squares = 1

#with middle points MARK: NOT SET TO 1 WHEN PLAN TO USE DUNE CODE --> wrong boundary cond.
with_middle_points = 0

#adjust meshsize
meshsize_top = 0.5
meshsize_bottom = 0.5
meshsize_bottom_square = 1
meshsize_pore = 0.05
meshsize_pore_square_around = 0.1

#size of the square at the bottom
diameter_bottom_square = 2 #need to be smaller than diameter


#needed variables---DO NOT CHANGE
radius = diameter/2
radius_bottom_square = diameter_bottom_square/2
pores_placing= diameter/(pores_per_side*2)
radius_pores = diameter_pores/2
space_around_the_pores = (diamamter_pores_around-diameter_pores)/2
radius_pores_around = radius_pores + space_around_the_pores
alpha = 360/polygon_corners
alpha_rad = math.radians(alpha)
beta = (180-alpha)/2
beta_rad = math.radians(beta)


#Middle Points for Pore-Placing----------
middle_points_x = []
middle_points_y = []
    
#points for bottom-Base of zylinder--------
bottom_points = []
bottom_lines = []
sidepoint_pores_x = []
sidepoint_pores_y = []
sidepoint_pores_x_around = []
sidepoint_pores_y_around = []

#Compute bottom-point and x,y-coordinates for Corners of Pores-----------------
side_length = 0
Factor_1 = 0    #Factor for point before Pore-middlepoint
Factor_2 = 0    #Factor for point behind Pore-middlepoint

Factor_1_around = 0    #Factor for point before Pore-middlepoint -around
Factor_2_around = 0    #Factor for point behind Pore-middlepoint -around

for i in range(0, polygon_corners):
    bottom_points.append(gmsh.model.geo.add_point(math.cos(i*alpha_rad)*radius + origin[0], math.sin(i*alpha_rad)*radius + origin[1] , 0, meshsize_bottom))
    if(i >=1):
        bottom_lines.append(gmsh.model.geo.add_line(bottom_points[i-1], bottom_points[i]))
        
        #pore corner_points
        A = [math.cos((i-1)*alpha_rad)*radius + origin[0], math.sin((i-1)*alpha_rad)*radius + origin[1], 0]
        B = [math.cos(i*alpha_rad)*radius + origin[0], math.sin(i*alpha_rad)*radius + origin[1], 0]
        #print(A)
        #print(B)
        if (i== 1):
            side_length = math.sqrt((B[0]-A[0])*(B[0]-A[0]) + (B[1]-A[1])*(B[1]-A[1]) + (B[2]-A[2])*(B[2]-A[2]))
            print('side_length = ', side_length)
            Factor_1 = (1/side_length)*((side_length/2)-radius_pores)
            Factor_2 = (1/side_length)*((side_length/2)+radius_pores)
            
            if(with_squares == 1):
                Factor_1_around = (1/side_length)*((side_length/2)-radius_pores_around)
                Factor_2_around = (1/side_length)*((side_length/2)+radius_pores_around)
            #print(Factor_1, 'and', Factor_1_around)
            
        sidepoint_pores_x.append(A[0] + Factor_1*(B[0]-A[0]))
        sidepoint_pores_y.append(A[1] + Factor_1*(B[1]-A[1]))
        sidepoint_pores_x.append(A[0] + Factor_2*(B[0]-A[0]))
        sidepoint_pores_y.append(A[1] + Factor_2*(B[1]-A[1]))
        
        if(with_squares == 1):
            sidepoint_pores_x_around.append(A[0] + Factor_1_around*(B[0]-A[0]))
            sidepoint_pores_y_around.append(A[1] + Factor_1_around*(B[1]-A[1]))
            sidepoint_pores_x_around.append(A[0] + Factor_2_around*(B[0]-A[0]))
            sidepoint_pores_y_around.append(A[1] + Factor_2_around*(B[1]-A[1]))
        
        if(with_middle_points == 1):
            middle_points_x.append(math.cos(i*alpha_rad+(alpha_rad/2))* (math.sin(beta_rad)*radius) + origin[0])
            middle_points_y.append(math.sin(i*alpha_rad+(alpha_rad/2))* (math.sin(beta_rad)*radius) + origin[1])
        
    
A = [math.cos((polygon_corners-1)*alpha_rad)*radius + origin[0], math.sin((polygon_corners-1)*alpha_rad)*radius + origin[1], 0]
B = [math.cos((0)*alpha_rad)*radius + origin[0], math.sin((0)*alpha_rad)*radius + origin[1], 0]
sidepoint_pores_x.append(A[0] + Factor_1*(B[0]-A[0]))
sidepoint_pores_y.append(A[1] + Factor_1*(B[1]-A[1]))
sidepoint_pores_x.append(A[0] + Factor_2*(B[0]-A[0]))
sidepoint_pores_y.append(A[1] + Factor_2*(B[1]-A[1]))

if(with_squares == 1):
    sidepoint_pores_x_around.append(A[0] + Factor_1_around*(B[0]-A[0]))
    sidepoint_pores_y_around.append(A[1] + Factor_1_around*(B[1]-A[1]))
    sidepoint_pores_x_around.append(A[0] + Factor_2_around*(B[0]-A[0]))
    sidepoint_pores_y_around.append(A[1] + Factor_2_around*(B[1]-A[1]))


bottom_lines.append(gmsh.model.geo.add_line(bottom_points[polygon_corners-1], bottom_points[0]))
bottom_curveloop = gmsh.model.geo.addCurveLoop(bottom_lines)

#Compute porecount on whole nucleus-----------
#surface_area
pores_on_M = polygon_corners * pores_per_side
SA_base = (polygon_corners/2)*radius*radius*math.sin(alpha_rad)
M = polygon_corners*side_length*diameter
G = M + 2*SA_base

percentage_M = (100/G)*M
pore_count = (pores_on_M/percentage_M)*100
print('Pores on nucleus = ', int(pore_count))


#buttom_square -------------------------
P1 = gmsh.model.geo.add_point(-radius_bottom_square, -radius_bottom_square, 0, meshsize_bottom_square)
P2 = gmsh.model.geo.add_point(radius_bottom_square, -radius_bottom_square, 0, meshsize_bottom_square)
P3 = gmsh.model.geo.add_point(radius_bottom_square, radius_bottom_square, 0, meshsize_bottom_square)
P4 = gmsh.model.geo.add_point(-radius_bottom_square, radius_bottom_square, 0, meshsize_bottom_square)


line1 = gmsh.model.geo.add_line(P1, P2)
line2 = gmsh.model.geo.add_line(P2, P3)
line3 = gmsh.model.geo.add_line(P3, P4)
line4 = gmsh.model.geo.add_line(P4, P1)

bottom_square_curve_loop = gmsh.model.geo.addCurveLoop([line1, line2, line3, line4])
square_surface = gmsh.model.geo.add_plane_surface([bottom_square_curve_loop])
bottom_surface = gmsh.model.geo.add_plane_surface([bottom_curveloop, -bottom_square_curve_loop])


#points for top-Base of zylinder-------------
top_points = []
top_lines = []
for i in range(0, polygon_corners):
    top_points.append(gmsh.model.geo.add_point(math.cos(i*alpha_rad)*radius + origin[0], math.sin(i*alpha_rad)*radius + origin[1] , diameter, meshsize_top))
    if(i >=1):
        top_lines.append(gmsh.model.geo.add_line(top_points[i-1], top_points[i]))
        
top_lines.append(gmsh.model.geo.add_line(top_points[polygon_corners-1], top_points[0]))
top_curveloop = gmsh.model.geo.addCurveLoop(top_lines)
top_surface = gmsh.model.geo.add_plane_surface([top_curveloop])


#lines on Side
side_lines = []
for i in range(0, polygon_corners):
    side_lines.append(gmsh.model.geo.add_line(bottom_points[i], top_points[i]))


#Pores on the sides---------------------------------

if(with_middle_points == 1):
    middle_points_pores = []

    ##print(pores_placing)
    for i in range(0, len(middle_points_x)):
        for j in range(0, pores_per_side*2):
            if(j % 2 == 1):
                middle_points_pores.append(gmsh.model.geo.add_point(middle_points_x[i], middle_points_y[i], j*pores_placing))
#
#        print(j)

#CornerCoordinates Pores-----------------------------
side_pores= []
side_lines_bottom_top= []
side_lines_left_right = []
pores_curve_loop = []
pores_surface = []
for i in range(0, len(sidepoint_pores_x)):
    for j in range(0, pores_per_side*2):
        if(j % 2 == 1):
            side_pores.append(gmsh.model.geo.add_point(sidepoint_pores_x[i], sidepoint_pores_y[i], j* pores_placing - radius_pores, meshsize_pore))
            side_pores.append(gmsh.model.geo.add_point(sidepoint_pores_x[i], sidepoint_pores_y[i], j* pores_placing + radius_pores, meshsize_pore))
            
            side_lines_left_right.append(gmsh.model.geo.add_line( side_pores[-2], side_pores[-1]))
index = 0
for j in range (0, polygon_corners):
    for i in range(0, pores_per_side*2):
        side_lines_bottom_top.append(gmsh.model.geo.add_line(side_pores[index+i],side_pores[index+i + pores_per_side*2]))
    index += pores_per_side*4

side_index = 0
next_side = 0
for i in range(0,2*pores_per_side*polygon_corners-1):
    if (i == 0):
        pores_curve_loop.append(gmsh.model.geo.addCurveLoop([side_lines_bottom_top[0], side_lines_left_right[pores_per_side], -side_lines_bottom_top[1], -side_lines_left_right[0]]))
        pores_surface.append(gmsh.model.geo.add_plane_surface([pores_curve_loop[0]]))
        
    if(i >= 2 and i%2 == 0):
        side_index = int(i/2)%pores_per_side
        if (i%(pores_per_side*2) == 0):
            next_side += pores_per_side*2
        
        #print(next_side)
        #print(side_index)
        #print(i, ',' , next_side+side_index+pores_per_side, ',', -(i+1) , ',', next_side+side_index)
        pores_curve_loop.append(gmsh.model.geo.addCurveLoop([side_lines_bottom_top[i], side_lines_left_right[next_side+side_index+pores_per_side], -side_lines_bottom_top[i+1], -side_lines_left_right[next_side+side_index]]))
  
        pores_surface.append( gmsh.model.geo.add_plane_surface([pores_curve_loop[-1]]))

print('pores_surfaces', pores_surface)

#Points around Pores----------------------------------------
if(with_squares == 1):
    side_pores_around= []
    side_lines_bottom_top_around= []
    side_lines_left_right_around = []
    pores_curve_loop_around= []
    pores_surface_around = []

    for i in range(0, len(sidepoint_pores_x_around)):
        for j in range(0, pores_per_side*2):
            if(j % 2 == 1):
                side_pores_around.append(gmsh.model.geo.add_point(sidepoint_pores_x_around[i], sidepoint_pores_y_around[i], j* pores_placing - radius_pores -space_around_the_pores, meshsize_pore_square_around))
                side_pores_around.append(gmsh.model.geo.add_point(sidepoint_pores_x_around[i], sidepoint_pores_y_around[i], j* pores_placing + radius_pores +space_around_the_pores, meshsize_pore_square_around))
        
                side_lines_left_right_around.append(gmsh.model.geo.add_line( side_pores_around[-2], side_pores_around[-1]))
    index = 0
    for j in range (0, polygon_corners):
        for i in range(0, pores_per_side*2):
            side_lines_bottom_top_around.append(gmsh.model.geo.add_line(side_pores_around[index+i],side_pores_around[index+i + pores_per_side*2]))
        index += pores_per_side*4
        

    side_index = 0
    next_side = 0
    pore_counter = 0
    for i in range(0,2*pores_per_side*polygon_corners-1):
        if (i == 0):
            pores_curve_loop_around.append(gmsh.model.geo.addCurveLoop([side_lines_bottom_top_around[0], side_lines_left_right_around[pores_per_side], -side_lines_bottom_top_around[1], -side_lines_left_right_around[0]]))
            pores_surface_around.append(gmsh.model.geo.add_plane_surface([pores_curve_loop_around[0],pores_curve_loop[pore_counter]]))
            pore_counter +=1
            
        if(i >= 2 and i%2 == 0):
            side_index = int(i/2)%pores_per_side
            if (i%(pores_per_side*2) == 0):
                next_side += pores_per_side*2
            
            #print(next_side)
            #print(side_index)
            #print(i, ',' , next_side+side_index+pores_per_side, ',', -(i+1) , ',', next_side+side_index)
            pores_curve_loop_around.append(gmsh.model.geo.addCurveLoop([side_lines_bottom_top_around[i], side_lines_left_right_around[next_side+side_index+pores_per_side], -side_lines_bottom_top_around[i+1], -side_lines_left_right_around[next_side+side_index]]))
        
            pores_surface_around.append( gmsh.model.geo.add_plane_surface([pores_curve_loop_around[-1], pores_curve_loop[pore_counter]]))
            pore_counter +=1
        
    print('pores_surfaces_around',pores_surface_around)
#sidesurface - cut out pore-loops---------------------------
side_surfaces = []

for i in range(0,polygon_corners):
    pores_on_this_side = []
    if(i<polygon_corners-1):
        if(with_squares == 0):
            side_curveloop = gmsh.model.geo.addCurveLoop([bottom_lines[i], side_lines[i+1], -top_lines[i], -side_lines[i]])
            for j in range(0,pores_per_side):
                pores_on_this_side.append(pores_curve_loop[j+i*pores_per_side])
        else:
            side_curveloop = gmsh.model.geo.addCurveLoop([bottom_lines[i], side_lines[i+1], -top_lines[i], -side_lines[i]])
            for j in range(0,pores_per_side):
                pores_on_this_side.append(pores_curve_loop[j+i*pores_per_side])
                pores_on_this_side.append(pores_curve_loop_around[j+i*pores_per_side])
        pores_on_this_side_negative = [ -x for x in pores_on_this_side ]
        #print('pores_on_this_side = ', pores_on_this_side)

        side_surfaces.append(gmsh.model.geo.add_plane_surface([side_curveloop, *pores_on_this_side_negative]))
    else:
        side_curveloop = gmsh.model.geo.addCurveLoop([bottom_lines[i], side_lines[0], -top_lines[i], -side_lines[i]])
        pores_on_last_side = []
        for j in range(1,pores_per_side+1):
            if(with_squares == 0):
                pores_on_last_side.append(pores_curve_loop[-j])
            else:
                pores_on_last_side.append(pores_curve_loop_around[-j])

        pores_on_last_side_negative = [ -x for x in pores_on_last_side]
        side_surfaces.append(gmsh.model.geo.add_plane_surface([side_curveloop, *pores_on_last_side_negative]))

print('side_surfaces',side_surfaces)
#Add volume
surface_set = []

if(with_squares == 0):
    for i in range(1, 3+ polygon_corners + polygon_corners*pores_per_side +1):
        surface_set.append(i)
else:
    for i in range(1, 3+ polygon_corners + polygon_corners*pores_per_side*2 +1):
        surface_set.append(i)

surface_loop = gmsh.model.geo.addSurfaceLoop(surface_set)



#for i in range(1, 3+ polygon_corners + polygon_corners*pores_per_side +1):
#    surface_set.append(i)
#surface_loop = gmsh.model.geo.addSurfaceLoop(surface_set)

    
    
if(with_volume ==1):
    gmsh.model.geo.addVolume([surface_loop])


#print(gmsh.model.getValue(0, 2, []))

# Create the relevant Gmsh data structures
# from Gmsh model.
gmsh.model.geo.synchronize()



#MARK: only can be done after syncronize()
#Where Dirichlet boundary consditions should be ?

#Points in Diricletcond.txt

#u12
P1_coordinates = gmsh.model.getValue(0, polygon_corners+1 ,[])
P3_coordinates = gmsh.model.getValue(0, polygon_corners+3 ,[])
#print('P1_coordinates =', P1_coordinates)
#print('P3_corrdinates =', P3_coordinates)
text.write('Eingeben für Dirichlet für u12:')
text.write('\n')
text.write('x[2] < ')
text.write(str(accuracy))
text.write(' and x[0]> ')
text.write(str(P1_coordinates[0]))
text.write('+ ')
text.write(str(accuracy))
text.write(' and x[0] < ')
text.write(str(P3_coordinates[0]))
text.write('- ')
text.write(str(accuracy))
text.write(' and x[1] > ')
text.write(str(P1_coordinates[1]))
text.write('+ ')
text.write(str(accuracy))
text.write(' and x[1] < ')
text.write(str(P3_coordinates[1]))
text.write('-')
text.write(str(accuracy))
text.write('\n')
text.write('-------------------------------- \n')

#u3
text.write('Eingeben für Dirichlet für u3:')
text.write('\n')
text.write('x[2] < ')
text.write(str(accuracy))
text.write(' or x[2]> ')
text.write(str(diameter - accuracy))
text.write('\n')
text.write('-------------------------------- \n')

text.write('Erstes Or nicht mitkopieren!!!!\n')

#dircletp
#points before pores
pbp = polygon_corners*2+4
side_change = 0
for pore in range(1,polygon_corners*pores_per_side+1):
    if(pore != 1 and pore%pores_per_side == 1):
        side_change = side_change + pores_per_side*2 #wechselt auf nächste Seite
    #print('A =', pbp +1 +(pore-1)*2+side_change  )
    #print('B =', pbp +1 +(pore-1)*2+side_change+pores_per_side*2+1)
    A = gmsh.model.getValue(0, pbp +1 +(pore-1)*2+side_change,[]) #dimension, tag(integer),vector
    D = gmsh.model.getValue(0, pbp +1 +(pore-1)*2+side_change+pores_per_side*2+1,[])
    

    print("A =", A)
    print("D =",D)
        
    
    if(A[0] >= D[0]-accuracy and A[0] <= D[0]+accuracy):
        text.write(' or x[0] >  ')
        text.write(str(D[0]))
        text.write('- ')
        text.write(str(accuracy))
        text.write(' and x[0] <  ')
        text.write(str(D[0]))
        text.write('+ ')
        text.write(str(accuracy))
    elif(A[0]<D[0]):
        text.write(' or x[0]> ')
        text.write(str(A[0]))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[0] < ')
        text.write(str(D[0]))
        text.write('- ')
        text.write(str(accuracy))
    else:
        text.write(' or x[0]> ')
        text.write(str(D[0]))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[0] < ')
        text.write(str(A[0]))
        text.write('- ')
        text.write(str(accuracy))
        
        
    if(A[1] >= D[1]-accuracy and A[1] <= D[1]+accuracy):
        text.write(' and x[1] >  ')
        text.write(str(D[1]))
        text.write('- ')
        text.write(str(accuracy))
        text.write(' and x[1] <  ')
        text.write(str(D[1]))
        text.write('+ ')
        text.write(str(accuracy))
        #if(A[1]<0):
        #print('yaaaaaaaaaaaaasskkkksksks')
    elif(A[1]<D[1]):
        text.write(' and x[1]> ')
        text.write(str(A[1]))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[1] < ')
        text.write(str(D[1]))
        text.write('- ')
        text.write(str(accuracy))
        #text.write(' and x[1] == ')
        #text.write(str(D[1]))
            #text.write('+ ')
            #text.write(str(accuracy))
        #else:
         #  text.write(' and x[1] > ')
          #  text.write(str(D[1]))
          #  text.write('- ')
          #  text.write(str(accuracy))
    else:
        text.write(' and x[1]> ')
        text.write(str(D[1]))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[1] < ')
        text.write(str(A[1]))
        text.write('- ')
        text.write(str(accuracy))
    #print(D[2])
    if(A[2]<D[2]):
        text.write(' and x[2]> ')
        text.write(str(A[2]))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[2] < ')
        text.write(str(D[2]))
        text.write('- ')
        text.write(str(accuracy))
        text.write('\n')
    else:
        text.write(' and x[2]> ')
        text.write(str(D[2]))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[2] < ')
        text.write(str(A[2]))
        text.write('- ')
        text.write(str(accuracy))
        text.write('\n')
        

    
# Generate mesh:
gmsh.model.mesh.generate(dim = 3)
gmsh.option.setNumber("Mesh.MshFileVersion", 2.0) #in Dune:gmshreader nur für version 2 von gmsh
# Write mesh data:
gmsh.write("Zellmodell.msh2")
gmsh.write("Zellmodell.msh")
# Creates  graphical user interface
if 'close' not in sys.argv:
    gmsh.fltk.run()
  
# It finalize the Gmsh API
gmsh.finalize()
