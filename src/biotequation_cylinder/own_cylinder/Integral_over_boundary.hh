#ifdef HAVE_CONFIG_H
#define HAVE_CONFIG_H


#include "config.h"

 
#include <iostream>
 
#include <dune/common/filledarray.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/yaspgrid.hh>
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "dune/grid/io/file/vtk/vtksequencewriter.hh"
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab.hh>

template<typename GF>
double integrateGridFunction_boundary(const GF& gf, unsigned qorder = 1, double factor = 1, int infoflag = 0) {
    typedef typename GF::Traits::RangeType Range;
    
    Range val;
    double val_skalar;
    double sum = 0.0;

       for(const auto& cell : elements(gf.getGridView())) {
           for(const auto& intersection : intersections(gf.getGridView(),cell)){
            // Geometry of intersection
               auto ig = intersection.geometry();
               constexpr auto intersectionDim = decltype(ig)::mydimension;
               
            //Is there a intersection with domainboundary?
               bool b = intersection.boundary();
//               std::cout << "intersection with domain boundary " << b << std::endl;
               const auto& quadRule = Dune::QuadratureRules<double,intersectionDim>::rule(ig.type(), qorder);
               auto center = ig.center();
               
               for (const auto& quadPoint :quadRule){
                   if(b == false)
                   {
//                       std::cout << "NOT BOUNDARY"  << std::endl;
                       continue;
                   }else if(center[2] == 0 or center[2] == 1)
                   {
//                       std::cout << "UPPER OR LOWER BOUNDARY"<< center << std::endl;
                       continue;
                   }else{
                       auto global_x = ig.global(quadPoint.position());
                       auto unit_outer_normal = intersection.unitOuterNormal(quadPoint.position());
                       if(infoflag == 1){
                           std::cout << "OUTER NORMAL: " << unit_outer_normal << std::endl;
                       }
                       gf.evaluate(cell, global_x, val);
                       val_skalar = factor *val.operator*(unit_outer_normal);
                       val_skalar *= quadPoint.weight() * ig.integrationElement(quadPoint.position());
                       if(infoflag == 1){
                           std::cout << "Boundary-integral for this Element in normal direction: " << val_skalar << std::endl;
                       }

                       sum += val_skalar;
                   }
               }
           }
       }
    return sum;
    }

#endif
