#!/usr/bin/env python

# Import modules:
import gmsh
import sys
import math

#wirte txt file
#"x" - Create - will create a file, returns an error if the file exist
#"a" - Append - will create a file if the specified file does not exist
#"w" - Write - will create a file if the specified file does not exist
text = open("Dirichletcond.txt", "w")

  
# Initialize gmsh:
gmsh.initialize()

#WERTE:
LX = 20;
LZ = LX;
LY = LX;
inner_cube = 0
grid_pores = 1
LX_c=LX/1.2
diameter_pores = 0.05;
radius = diameter_pores/2;

diameter_grid_pores = 1
radius_grid = diameter_grid_pores/2

radius_u1 = 2

count_ofPores_per_side = 11;

meshsize_outer = 5
meshsize_pores = 1
meshsize_grid_pores = 1
meshsize_inner = 4
meshsize_u1= 5
accuracy = 1e-8

text.write('Poren pro Seite:')
text.write(str(count_ofPores_per_side))
text.write('\n')
text.write('Meshsize_outer:')
text.write(str(meshsize_outer))
text.write('\n')
text.write('meshsize_pores:')
text.write(str(meshsize_pores))
text.write('\n')
text.write('-------------------------------- \n')

# cube points:
P1 = gmsh.model.geo.add_point(0, 0, 0, meshsize_outer)
P2 = gmsh.model.geo.add_point(LX, 0, 0, meshsize_outer)
P3 = gmsh.model.geo.add_point(LX, LY, 0, meshsize_outer)
P4 = gmsh.model.geo.add_point(0, LY, 0, meshsize_outer)
P5 = gmsh.model.geo.add_point(0, LY, LZ, meshsize_outer)
P6 = gmsh.model.geo.add_point(0, 0, LZ, meshsize_outer)
P7 = gmsh.model.geo.add_point(LX, 0, LZ, meshsize_outer)
P8 = gmsh.model.geo.add_point(LX, LY, LZ, meshsize_outer)

# Edge of cube:
#groundfacelines
line1 = gmsh.model.geo.add_line(P1, P2)
line2 = gmsh.model.geo.add_line(P2, P3)
line3 = gmsh.model.geo.add_line(P3, P4)
line4 = gmsh.model.geo.add_line(P4, P1)

#upperface-lines
line5 = gmsh.model.geo.add_line(P5, P6)
line6 = gmsh.model.geo.add_line(P6, P7)
line7 = gmsh.model.geo.add_line(P7, P8)
line8 = gmsh.model.geo.add_line(P8, P5)

#restliche-lines
line9 = gmsh.model.geo.add_line(P4, P5)
line10 = gmsh.model.geo.add_line(P6, P1)
line11 = gmsh.model.geo.add_line(P7, P2)
line12 = gmsh.model.geo.add_line(P3, P8)


surfaces = []
surfaces2 = []

#u1 on the ground
Middlepointx=LX/2
Middlepointy=LX/2
points_u1= []
line_u1=[]
curve_u1=[]

points_u1.append(gmsh.model.geo.add_point(Middlepointx + radius_u1, Middlepointy + radius_u1,0, meshsize_u1))
points_u1.append(gmsh.model.geo.add_point(Middlepointx - radius_u1, Middlepointy + radius_u1,0, meshsize_u1))
points_u1.append(gmsh.model.geo.add_point(Middlepointx - radius_u1,  Middlepointy - radius_u1,0, meshsize_u1))
points_u1.append(gmsh.model.geo.add_point(Middlepointx + radius_u1, Middlepointy - radius_u1,0, meshsize_u1))

text.write('Eingeben für Dirichlet für u12:')
text.write('\n')
text.write('x[2] < ')
text.write(str(accuracy))
text.write(' and x[0]> ')
text.write(str(Middlepointx - radius_u1))
text.write('+ ')
text.write(str(accuracy))
text.write(' and x[0] < ')
text.write(str(Middlepointx + radius_u1))
text.write('- ')
text.write(str(accuracy))
text.write(' and x[1] > ')
text.write(str(Middlepointy - radius_u1))
text.write('+ ')
text.write(str(accuracy))
text.write(' and x[1] < ')
text.write(str(Middlepointy + radius_u1))
text.write('-')
text.write(str(accuracy))
text.write('\n')
text.write('-------------------------------- \n')

text.write('Eingeben für Dirichlet für u3:')
text.write('\n')
text.write('x[2] < ')
text.write(str(accuracy))
text.write(' or x[2]> ')
text.write(str(LX - accuracy))
text.write('\n')
text.write('-------------------------------- \n')

text.write('Letztes Or nicht mitkopieren!!!!\n')


line_u1.append(gmsh.model.geo.add_line(points_u1[-4], points_u1[-3]))
line_u1.append(gmsh.model.geo.add_line(points_u1[-3], points_u1[-2]))
line_u1.append(gmsh.model.geo.add_line(points_u1[-2], points_u1[-1]))
line_u1.append(gmsh.model.geo.add_line(points_u1[-1], points_u1[-4]))

curve_u1.append(gmsh.model.geo.addCurveLoop([line_u1[-4], line_u1[-3],line_u1[-2], line_u1[-1]]))
surfaces.append(gmsh.model.geo.add_plane_surface([curve_u1[-1]]))


#Poren
placing = LX/(count_ofPores_per_side*2)
text.write('-------------------------------- \n')
#Für x=0
points_x0 =[]
points_grid_x0 = []

line_x0 = []
line_grid_x0 = []

curve_x0 =[]
curve_grid_x0 =[]

surfaces_grid_x0 =[]
i=1
k=1
while i < count_ofPores_per_side*2:
    while k < count_ofPores_per_side*2:
        Midpointx = placing * i
        Midpointy = placing * k
        print('k = ' , k, 'i = ' , i)
        print( '0.0,', Midpointx, ',', Midpointy )
        
        if (count_ofPores_per_side == 11 and (i == 1 or i == 7 or i == 11 or i == 15 or i == 21) and (k == 1 or k == 7 or k == 11 or k == 15 or k == 21)):
            print('Middlepoint at k = ', k, 'and i = ', i, ':')
            print( 0, ',', Midpointx, ',', Midpointy)
            text.write('-------------------------------- \n')
            
        # Poren
        points_x0.append(gmsh.model.geo.add_point(0, Midpointx + radius, Midpointy + radius, meshsize_pores))
        points_x0.append(gmsh.model.geo.add_point(0, Midpointx - radius, Midpointy + radius, meshsize_pores))
        points_x0.append(gmsh.model.geo.add_point(0, Midpointx - radius, Midpointy - radius, meshsize_pores))
        points_x0.append(gmsh.model.geo.add_point(0, Midpointx + radius, Midpointy - radius, meshsize_pores))
#        print("i = ",i)
#        print("k = " , k)
#        print(points_x0)
        
        if(grid_pores ==1):
        #grid um Poren
            points_grid_x0.append(gmsh.model.geo.add_point(0, Midpointx + radius_grid, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_x0.append(gmsh.model.geo.add_point(0, Midpointx - radius_grid, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_x0.append(gmsh.model.geo.add_point(0, Midpointx - radius_grid, Midpointy - radius_grid, meshsize_grid_pores))
            points_grid_x0.append(gmsh.model.geo.add_point(0, Midpointx + radius_grid, Midpointy - radius_grid, meshsize_grid_pores))
        
        #line poren
        line_x0.append(gmsh.model.geo.add_line(points_x0[-4], points_x0[-3]))
        line_x0.append(gmsh.model.geo.add_line(points_x0[-3], points_x0[-2]))
        line_x0.append(gmsh.model.geo.add_line(points_x0[-2], points_x0[-1]))
        line_x0.append(gmsh.model.geo.add_line(points_x0[-1], points_x0[-4]))
        #print(line_x0)
        
        if(grid_pores ==1):
        #line grid um poren
            line_grid_x0.append(gmsh.model.geo.add_line(points_grid_x0[-4], points_grid_x0[-3]))
            line_grid_x0.append(gmsh.model.geo.add_line(points_grid_x0[-3], points_grid_x0[-2]))
            line_grid_x0.append(gmsh.model.geo.add_line(points_grid_x0[-2], points_grid_x0[-1]))
            line_grid_x0.append(gmsh.model.geo.add_line(points_grid_x0[-1], points_grid_x0[-4]))

        #curve poren
        curve_x0.append(gmsh.model.geo.addCurveLoop([line_x0[-4], line_x0[-3],line_x0[-2], line_x0[-1]]))
        #print(curve_x0)
        
        if(grid_pores ==1):
        #curve grid um poren
            curve_grid_x0.append(gmsh.model.geo.addCurveLoop([line_grid_x0[-4], line_grid_x0[-3],line_grid_x0[-2], line_grid_x0[-1]]))
        
            #negative_curve_grid_x0 = [ -x for x in curve_x0 ]
            surfaces_grid_x0 = gmsh.model.geo.add_plane_surface([curve_grid_x0[-1], curve_x0[-1]])
            
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_x0[-1]]))
            surfaces.append(surfaces_grid_x0)
        else:
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_x0[-1]]))

        text.write('\n')
        text.write('(x[0] < ')
        text.write(str(accuracy))
        text.write(' and x[1] > ')
        text.write(str(Midpointx - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[1] < ')
        text.write(str(Midpointx + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(' and x[2] > ')
        text.write(str(Midpointy - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[2] < ')
        text.write(str(Midpointy + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(')')
        text.write('\n')
        text.write('or')



        k= k+2
    k=1
    i= i+2

#Für x=1

points_x1 =[]
points_grid_x1 = []

line_x1 = []
line_grid_x1 = []

curve_x1 =[]
curve_grid_x1 =[]

surfaces_grid_x1 =[]

i=1
k=1
while i < count_ofPores_per_side*2:
    while k < count_ofPores_per_side*2:
        Midpointx = placing * i
        Midpointy = placing * k
            
        points_x1.append(gmsh.model.geo.add_point(LX, Midpointx + radius, Midpointy + radius, meshsize_pores))
        points_x1.append(gmsh.model.geo.add_point(LX, Midpointx - radius, Midpointy + radius, meshsize_pores))
        points_x1.append(gmsh.model.geo.add_point(LX, Midpointx - radius, Midpointy - radius, meshsize_pores))
        points_x1.append(gmsh.model.geo.add_point(LX, Midpointx + radius, Midpointy - radius, meshsize_pores))
#        print("i = ",i)
#        print("k = " , k)
#        print(points_x1)
        if(grid_pores ==1):
        #grid um Poren
            points_grid_x1.append(gmsh.model.geo.add_point(LX, Midpointx + radius_grid, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_x1.append(gmsh.model.geo.add_point(LX, Midpointx - radius_grid, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_x1.append(gmsh.model.geo.add_point(LX, Midpointx - radius_grid, Midpointy - radius_grid, meshsize_grid_pores))
            points_grid_x1.append(gmsh.model.geo.add_point(LX, Midpointx + radius_grid, Midpointy - radius_grid, meshsize_grid_pores))
            
        line_x1.append(gmsh.model.geo.add_line(points_x1[-4], points_x1[-3]))
        line_x1.append(gmsh.model.geo.add_line(points_x1[-3], points_x1[-2]))
        line_x1.append(gmsh.model.geo.add_line(points_x1[-2], points_x1[-1]))
        line_x1.append(gmsh.model.geo.add_line(points_x1[-1], points_x1[-4]))
#        print(line_x1)

        if(grid_pores ==1):
        #line grid um poren
            line_grid_x1.append(gmsh.model.geo.add_line(points_grid_x1[-4], points_grid_x1[-3]))
            line_grid_x1.append(gmsh.model.geo.add_line(points_grid_x1[-3], points_grid_x1[-2]))
            line_grid_x1.append(gmsh.model.geo.add_line(points_grid_x1[-2], points_grid_x1[-1]))
            line_grid_x1.append(gmsh.model.geo.add_line(points_grid_x1[-1], points_grid_x1[-4]))

        curve_x1.append(gmsh.model.geo.addCurveLoop([line_x1[-4], line_x1[-3],line_x1[-2], line_x1[-1]]))
#        print(curve_x1)

        if(grid_pores ==1):
        #curve grid um poren
            curve_grid_x1.append(gmsh.model.geo.addCurveLoop([line_grid_x1[-4], line_grid_x1[-3],line_grid_x1[-2], line_grid_x1[-1]]))
        
            #negative_curve_grid_x0 = [ -x for x in curve_x0 ]
            surfaces_grid_x1 = gmsh.model.geo.add_plane_surface([curve_grid_x1[-1], curve_x1[-1]])
            
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_x1[-1]]))
            surfaces.append(surfaces_grid_x1)
        else:
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_x1[-1]]))

        text.write('\n')
        text.write('(x[0] > 1-')
        text.write(str(accuracy))
        text.write(' and x[1]>')
        text.write(str(Midpointx - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[1] < ')
        text.write(str(Midpointx + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(' and x[2] > ')
        text.write(str(Midpointy - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[2] < ')
        text.write(str(Midpointy + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(')')
        text.write('\n')
        text.write('or')

        k= k+2
    k=1
    i= i+2

#Für y=0
points_y0 =[]
points_grid_y0 = []

line_y0 = []
line_grid_y0 = []

curve_y0 =[]
curve_grid_y0 =[]

surfaces_grid_y0 =[]

i=1
k=1
while i < count_ofPores_per_side*2:
    while k < count_ofPores_per_side*2:
        Midpointx = placing * i
        Midpointy = placing * k

        points_y0.append(gmsh.model.geo.add_point(Midpointx + radius, 0, Midpointy + radius, meshsize_pores))
        points_y0.append(gmsh.model.geo.add_point(Midpointx - radius, 0, Midpointy + radius, meshsize_pores))
        points_y0.append(gmsh.model.geo.add_point(Midpointx - radius, 0, Midpointy - radius, meshsize_pores))
        points_y0.append(gmsh.model.geo.add_point(Midpointx + radius, 0, Midpointy - radius, meshsize_pores))
#        print("i = ",i)
#        print("k = " , k)
#        print(points_y0)
        if(grid_pores ==1):
            points_grid_y0.append(gmsh.model.geo.add_point(Midpointx + radius_grid, 0, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_y0.append(gmsh.model.geo.add_point(Midpointx - radius_grid, 0, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_y0.append(gmsh.model.geo.add_point(Midpointx - radius_grid, 0, Midpointy - radius_grid, meshsize_grid_pores))
            points_grid_y0.append(gmsh.model.geo.add_point(Midpointx + radius_grid, 0, Midpointy - radius_grid, meshsize_grid_pores))

        line_y0.append(gmsh.model.geo.add_line(points_y0[-4], points_y0[-3]))
        line_y0.append(gmsh.model.geo.add_line(points_y0[-3], points_y0[-2]))
        line_y0.append(gmsh.model.geo.add_line(points_y0[-2], points_y0[-1]))
        line_y0.append(gmsh.model.geo.add_line(points_y0[-1], points_y0[-4]))
#        print(line_y0)

        if(grid_pores ==1):
            line_grid_y0.append(gmsh.model.geo.add_line(points_grid_y0[-4], points_grid_y0[-3]))
            line_grid_y0.append(gmsh.model.geo.add_line(points_grid_y0[-3], points_grid_y0[-2]))
            line_grid_y0.append(gmsh.model.geo.add_line(points_grid_y0[-2], points_grid_y0[-1]))
            line_grid_y0.append(gmsh.model.geo.add_line(points_grid_y0[-1], points_grid_y0[-4]))
        

        curve_y0.append(gmsh.model.geo.addCurveLoop([line_y0[-4], line_y0[-3],line_y0[-2], line_y0[-1]]))
#        print(curve_y0)

        if(grid_pores ==1):
        #curve grid um poren
            curve_grid_y0.append(gmsh.model.geo.addCurveLoop([line_grid_y0[-4], line_grid_y0[-3],line_grid_y0[-2], line_grid_y0[-1]]))
        
            #negative_curve_grid_x0 = [ -x for x in curve_x0 ]
            surfaces_grid_y0 = gmsh.model.geo.add_plane_surface([curve_grid_y0[-1], curve_y0[-1]])
            
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_y0[-1]]))
            surfaces.append(surfaces_grid_y0)
        else:
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_y0[-1]]))

        text.write('\n')
        text.write('(x[1] < ')
        text.write(str(accuracy))
        text.write(' and x[0] > ')
        text.write(str(Midpointx - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[0] < ')
        text.write(str(Midpointx + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(' and x[2] > ')
        text.write(str(Midpointy - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[2] < ')
        text.write(str(Midpointy + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(')')
        text.write('\n')
        text.write('or')

        k= k+2
    k=1
    i= i+2
    
#Für y=1
points_y1 =[]
points_grid_y1 = []

line_y1 = []
line_grid_y1 = []

curve_y1 =[]
curve_grid_y1 =[]

surfaces_grid_y1 =[]
i=1
k=1
while i < count_ofPores_per_side*2:
    while k < count_ofPores_per_side*2:
        Midpointx = placing * i
        Midpointy = placing * k

        points_y1.append(gmsh.model.geo.add_point(Midpointx + radius, LX, Midpointy + radius, meshsize_pores))
        points_y1.append(gmsh.model.geo.add_point(Midpointx - radius, LX, Midpointy + radius, meshsize_pores))
        points_y1.append(gmsh.model.geo.add_point(Midpointx - radius, LX, Midpointy - radius, meshsize_pores))
        points_y1.append(gmsh.model.geo.add_point(Midpointx + radius, LX, Midpointy - radius, meshsize_pores))
#        print("i = ",i)
#        print("k = " , k)
#        print(points_y1)

        if(grid_pores ==1):
            points_grid_y1.append(gmsh.model.geo.add_point(Midpointx + radius_grid, LX, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_y1.append(gmsh.model.geo.add_point(Midpointx - radius_grid, LX, Midpointy + radius_grid, meshsize_grid_pores))
            points_grid_y1.append(gmsh.model.geo.add_point(Midpointx - radius_grid, LX, Midpointy - radius_grid, meshsize_grid_pores))
            points_grid_y1.append(gmsh.model.geo.add_point(Midpointx + radius_grid, LX, Midpointy - radius_grid, meshsize_grid_pores))

        line_y1.append(gmsh.model.geo.add_line(points_y1[-4], points_y1[-3]))
        line_y1.append(gmsh.model.geo.add_line(points_y1[-3], points_y1[-2]))
        line_y1.append(gmsh.model.geo.add_line(points_y1[-2], points_y1[-1]))
        line_y1.append(gmsh.model.geo.add_line(points_y1[-1], points_y1[-4]))
#        print(line_y1)

        if(grid_pores ==1):
            line_grid_y1.append(gmsh.model.geo.add_line(points_grid_y1[-4], points_grid_y1[-3]))
            line_grid_y1.append(gmsh.model.geo.add_line(points_grid_y1[-3], points_grid_y1[-2]))
            line_grid_y1.append(gmsh.model.geo.add_line(points_grid_y1[-2], points_grid_y1[-1]))
            line_grid_y1.append(gmsh.model.geo.add_line(points_grid_y1[-1], points_grid_y1[-4]))

        curve_y1.append(gmsh.model.geo.addCurveLoop([line_y1[-4], line_y1[-3],line_y1[-2], line_y1[-1]]))
#        print(curve_y1)
        if(grid_pores ==1):
        #curve grid um poren
            curve_grid_y1.append(gmsh.model.geo.addCurveLoop([line_grid_y1[-4], line_grid_y1[-3],line_grid_y1[-2], line_grid_y1[-1]]))
        
            #negative_curve_grid_x0 = [ -x for x in curve_x0 ]
            surfaces_grid_y1 = gmsh.model.geo.add_plane_surface([curve_grid_y1[-1], curve_y1[-1]])
            
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_y1[-1]]))
            surfaces.append(surfaces_grid_y1)
        else:
            surfaces.append(gmsh.model.geo.add_plane_surface([curve_y1[-1]]))

        text.write('\n')
        text.write('(x[1] > 1-')
        text.write(str(accuracy))
        text.write(' and x[0] > ')
        text.write(str(Midpointx - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[0] < ')
        text.write(str(Midpointx + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(' and x[2] > ')
        text.write(str(Midpointy - radius))
        text.write('+ ')
        text.write(str(accuracy))
        text.write(' and x[2] < ')
        text.write(str(Midpointy + radius))
        text.write('- ')
        text.write(str(accuracy))
        text.write(')')
        text.write('\n')
        text.write('or')

        k= k+2
    k=1
    i= i+2

if(inner_cube == 1):
    #Mittelwürfel
    points_c =[]
    line_c = []
    curve_c =[]

    points_c.append(gmsh.model.geo.add_point(LX_c, LX_c, LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX-LX_c, LX_c, LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX-LX_c, LY-LX_c, LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX_c, LY-LX_c, LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX_c, LY-LX_c, LZ-LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX_c, LX_c, LZ-LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX-LX_c, LX_c, LZ-LX_c, meshsize_inner))
    points_c.append(gmsh.model.geo.add_point(LX-LX_c, LY-LX_c, LZ-LX_c, meshsize_inner))

    # Edge of cube:
    #groundfacelines
    line_c.append(gmsh.model.geo.add_line(points_c[0], points_c[1]))
    line_c.append(gmsh.model.geo.add_line(points_c[1], points_c[2]))
    line_c.append(gmsh.model.geo.add_line(points_c[2], points_c[3]))
    line_c.append(gmsh.model.geo.add_line(points_c[3], points_c[0]))

    #upperface-lines
    line_c.append(gmsh.model.geo.add_line(points_c[4], points_c[5]))
    line_c.append(gmsh.model.geo.add_line(points_c[5], points_c[6]))
    line_c.append(gmsh.model.geo.add_line(points_c[6], points_c[7]))
    line_c.append(gmsh.model.geo.add_line(points_c[7], points_c[4]))

    #restliche-lines
    line_c.append(gmsh.model.geo.add_line(points_c[3], points_c[4]))
    line_c.append(gmsh.model.geo.add_line(points_c[5], points_c[0]))
    line_c.append(gmsh.model.geo.add_line(points_c[6], points_c[1]))
    line_c.append(gmsh.model.geo.add_line(points_c[2], points_c[7]))


    #schleife für gesammte surfaces
        #Surface gesammt


    curve_c.append(gmsh.model.geo.addCurveLoop([line_c[0], line_c[1],line_c[2], line_c[3]]))
    curve_c.append(gmsh.model.geo.addCurveLoop([line_c[4], line_c[5],line_c[6], line_c[7]]))
    curve_c.append(gmsh.model.geo.addCurveLoop([line_c[8], line_c[4],line_c[9], -line_c[3]]))
    curve_c.append(gmsh.model.geo.addCurveLoop([line_c[8], -line_c[7],-line_c[11], line_c[2]]))
    curve_c.append(gmsh.model.geo.addCurveLoop([line_c[5], line_c[10],-line_c[0], -line_c[9]]))
    curve_c.append(gmsh.model.geo.addCurveLoop([line_c[10], line_c[1],line_c[11], -line_c[6]]))

    surfaces2.append(gmsh.model.geo.add_plane_surface([curve_c[0]]))
    surfaces2.append(gmsh.model.geo.add_plane_surface([curve_c[1]]))
    surfaces2.append(gmsh.model.geo.add_plane_surface([curve_c[2]]))
    surfaces2.append(gmsh.model.geo.add_plane_surface([curve_c[3]]))
    surfaces2.append(gmsh.model.geo.add_plane_surface([curve_c[4]]))
    surfaces2.append(gmsh.model.geo.add_plane_surface([curve_c[5]]))


#schleife für gesammte surfaces
    #Surface gesammt

# faces of cube:
face1 = gmsh.model.geo.add_curve_loop([line1, line2, line3, line4])
face2 = gmsh.model.geo.add_curve_loop([line5, line6, line7, line8])
face3 = gmsh.model.geo.add_curve_loop([line9, line5, line10, -line4])
face4 = gmsh.model.geo.add_curve_loop([line9, -line8, -line12, line3])
face5 = gmsh.model.geo.add_curve_loop([line6, line11, -line1, -line10])
face6 = gmsh.model.geo.add_curve_loop([line11, line2, line12, -line7])

# surfaces of cube:

surface2 = gmsh.model.geo.add_plane_surface([face2])
surfaces.append(surface2)

#u1
negative_curve_u1 = [ -x for x in curve_u1 ]
surface1 = gmsh.model.geo.add_plane_surface([face1, *negative_curve_u1])
surfaces.append(surface1)

print("-----------------------")
if(grid_pores ==1):
    negative_curve_x0 = [ -x for x in curve_grid_x0 ]
    #print(negative_curve_x0)
    surface3_1 =  gmsh.model.geo.add_plane_surface([face3, *negative_curve_x0])
    surfaces.append(surface3_1)

    negative_curve_x1 = [ -x for x in curve_grid_x1 ]
    #print(negative_curve_x1)
    surface4_1 =  gmsh.model.geo.add_plane_surface([face6, *negative_curve_x1])
    surfaces.append(surface4_1)

    negative_curve_y0 = [ -x for x in curve_grid_y0 ]
    #print(negative_curve_y0)
    surface5_1 =  gmsh.model.geo.add_plane_surface([face5, *negative_curve_y0])
    surfaces.append(surface5_1)

    negative_curve_y1 = [ -x for x in curve_grid_y1 ]
    #print(negative_curve_y1)
    surface6_1 =  gmsh.model.geo.add_plane_surface([face4, *negative_curve_y1])
    surfaces.append(surface6_1)
else:
    negative_curve_x0 = [ -x for x in curve_x0 ]
#    print(negative_curve_x0)
    surface3_1 =  gmsh.model.geo.add_plane_surface([face3, *negative_curve_x0])
    surfaces.append(surface3_1)

    negative_curve_x1 = [ -x for x in curve_x1 ]
#    print(negative_curve_x1)
    surface4_1 =  gmsh.model.geo.add_plane_surface([face6, *negative_curve_x1])
    surfaces.append(surface4_1)

    negative_curve_y0 = [ -x for x in curve_y0 ]
#    print(negative_curve_y0)
    surface5_1 =  gmsh.model.geo.add_plane_surface([face5, *negative_curve_y0])
    surfaces.append(surface5_1)

    negative_curve_y1 = [ -x for x in curve_y1 ]
#    print(negative_curve_y1)
    surface6_1 =  gmsh.model.geo.add_plane_surface([face4, *negative_curve_y1])
    surfaces.append(surface6_1)


#innermesh
if(inner_cube ==1):
    surfaceloop2 = gmsh.model.geo.addSurfaceLoop([*surfaces2])
surfaceloop = gmsh.model.geo.addSurfaceLoop([*surfaces])
#print(surfaceloop)

if(inner_cube == 1):
    gmsh.model.geo.addVolume([surfaceloop2])
    gmsh.model.geo.addVolume([surfaceloop, -surfaceloop2])
else:
    gmsh.model.geo.addVolume([surfaceloop])
# Create the relevant Gmsh data structures
# from Gmsh model.
gmsh.model.geo.synchronize()
  
# Generate mesh:
gmsh.model.mesh.generate(dim = 3)
gmsh.option.setNumber("Mesh.MshFileVersion", 2.0) #in Dune:gmshreader nur für version 2 von gmsh
# Write mesh data:
gmsh.write("Zellmodell.msh2")
gmsh.write("Zellmodell.msh")
# Creates  graphical user interface
if 'close' not in sys.argv:
    gmsh.fltk.run()
  
# It finalize the Gmsh API
gmsh.finalize()
