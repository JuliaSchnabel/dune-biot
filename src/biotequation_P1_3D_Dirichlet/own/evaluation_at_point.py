import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.interpolate import make_interp_spline

read = []
time_1 = np.array([])
normal_value_1 = np.array([])

datei = open('Values_at_Point_23.txt', 'r')
lines = datei.readlines()
for line in lines:
    read.append([float(v) for v in line.split()])
    
print(read)

for i in range (0,len(read)):
    if i % 2 == 0:
        time_1 = np.append(time_1,read[i][0])
    else:
        normal_value_1 = np.append(normal_value_1, read[i][0])
    i = i+1

print(time_1)
print(normal_value_1)

#X_Y_Spline = make_interp_spline(time_1, normal_value_1)
#
#TIME = np.linspace(time_1.min(), time_1.max(), 500)
#VALUE = X_Y_Spline(TIME)
#fig = plt.figure()
#ax = fig.add_subplot(111)
#for i,j in zip(time_1,normal_value_1):
#    ax.annotate(str(j),xy=(i,j))
    
    
    
#plt.plot(time_1,normal_value_1, 'rx')
plt.title("Pore 15")
plt.ylim(0, 10)
plt.xlabel('Time in [s]')
plt.ylabel('Darcy-flow in [\u03bcm/s ]')
#plt.plot(TIME, VALUE)
plt.plot(time_1, normal_value_1)
print(plt.axis())
#plt.axis([time_1[0]+1, time_1[-1]+1, normal_value_1[0]+1, normal_value_1[-1]+1])
plt.show()
