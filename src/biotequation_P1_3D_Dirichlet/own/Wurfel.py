#!/usr/bin/env python

# Import modules:
import gmsh
import sys
import math

#wirte txt file
#"x" - Create - will create a file, returns an error if the file exist
#"a" - Append - will create a file if the specified file does not exist
#"w" - Write - will create a file if the specified file does not exist
text = open("Dirichletcond.txt", "w")

  
# Initialize gmsh:
gmsh.initialize()

#WERTE:
LX = 10;
LZ = LX;
LY = LX;
H = 0.1;
diameter_pores = 0.1;
radius = diameter_pores/2;
radius_u1 = 0.08
count_ofPores_per_side = 3;
meshsize_outer = 20*H;
meshsize_pores = 0.5*H;
meshsize_u1=1*H;
accuracy = 1e-8;

surfaces = []

text.write('Poren pro Seite:')
text.write(str(count_ofPores_per_side))
text.write('\n')
text.write('Meshsize_outer:')
text.write(str(meshsize_outer))
text.write('\n')
text.write('meshsize_pores:')
text.write(str(meshsize_pores))
text.write('\n')
text.write('-------------------------------- \n')

# cube points:
P1 = gmsh.model.geo.add_point(0, 0, 0, meshsize_outer)
P2 = gmsh.model.geo.add_point(LX, 0, 0, meshsize_outer)
P3 = gmsh.model.geo.add_point(LX, LY, 0, meshsize_outer)
P4 = gmsh.model.geo.add_point(0, LY, 0, meshsize_outer)
P5 = gmsh.model.geo.add_point(0, LY, LZ, meshsize_outer)
P6 = gmsh.model.geo.add_point(0, 0, LZ, meshsize_outer)
P7 = gmsh.model.geo.add_point(LX, 0, LZ, meshsize_outer)
P8 = gmsh.model.geo.add_point(LX, LY, LZ, meshsize_outer)
  
# Edge of cube:
#groundfacelines
line1 = gmsh.model.geo.add_line(P1, P2)
line2 = gmsh.model.geo.add_line(P2, P3)
line3 = gmsh.model.geo.add_line(P3, P4)
line4 = gmsh.model.geo.add_line(P4, P1)

#upperface-lines
line5 = gmsh.model.geo.add_line(P5, P6)
line6 = gmsh.model.geo.add_line(P6, P7)
line7 = gmsh.model.geo.add_line(P7, P8)
line8 = gmsh.model.geo.add_line(P8, P5)

#restliche-lines
line9 = gmsh.model.geo.add_line(P4, P5)
line10 = gmsh.model.geo.add_line(P6, P1)
line11 = gmsh.model.geo.add_line(P7, P2)
line12 = gmsh.model.geo.add_line(P3, P8)

#schleife für gesammte surfaces
    #Surface gesammt

# faces of cube:
face1 = gmsh.model.geo.add_curve_loop([line1, line2, line3, line4])
face2 = gmsh.model.geo.add_curve_loop([line5, line6, line7, line8])
face3 = gmsh.model.geo.add_curve_loop([line9, line5, line10, -line4])
face4 = gmsh.model.geo.add_curve_loop([line9, -line8, -line12, line3])
face5 = gmsh.model.geo.add_curve_loop([line6, line11, -line1, -line10])
face6 = gmsh.model.geo.add_curve_loop([line11, line2, line12, -line7])
  
# surfaces of cube:
surfaces.append(gmsh.model.geo.add_plane_surface([face1]))
surface2 = gmsh.model.geo.add_plane_surface([face2])
surfaces.append(surface2)

#u1
surfaces.append(gmsh.model.geo.add_plane_surface([face3]))
surfaces.append(gmsh.model.geo.add_plane_surface([face4]))
surfaces.append(gmsh.model.geo.add_plane_surface([face5]))
surfaces.append(gmsh.model.geo.add_plane_surface([face6]))


#innermesh
surfaceloop = gmsh.model.geo.addSurfaceLoop([*surfaces])
print(surfaceloop)
gmsh.model.geo.addVolume([surfaceloop])
# Create the relevant Gmsh data structures
# from Gmsh model.
gmsh.model.geo.synchronize()
  
# Generate mesh:
gmsh.model.mesh.generate(dim = 3)
gmsh.option.setNumber("Mesh.MshFileVersion", 2.0) #in Dune:gmshreader nur für version 2 von gmsh
# Write mesh data:
gmsh.write("wurfel.msh2")
  
# Creates  graphical user interface
if 'close' not in sys.argv:
    gmsh.fltk.run()
  
# It finalize the Gmsh API
gmsh.finalize()
