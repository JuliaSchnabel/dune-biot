#ifndef BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
#define BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH


#include "dune/pdelab/stationary/linearproblem.hh"
#include "dune/pdelab/backend/istl.hh"
#include "dune/pdelab/finiteelementmap/pkfem.hh"
#include "dune/pdelab/gridfunctionspace/powergridfunctionspace.hh"
#include "bioteq_r_operator.hh"
#include "biot_3D_di_massOperator_file.hh"
#include "dune/pdelab/gridoperator/gridoperator.hh"
#include "dune/pdelab/function/callableadapter.hh"
#include "dune/pdelab/constraints/conforming.hh"
#include "dune/codegen/vtkpredicate.hh"
#include "dune/grid/io/file/vtk/subsamplingvtkwriter.hh"
#include "string"
#include "dune/pdelab/gridfunctionspace/vtk.hh"
#include <random>
#include "dune/common/parametertree.hh"
#include "dune/common/parametertreeparser.hh"
#include "dune/pdelab/gridoperator/onestep.hh"
#include "dune/pdelab/newton/newton.hh"
#include "dune/pdelab/solver/newton.hh"
#include "dune/grid/io/file/vtk/vtksequencewriter.hh"
#include "dune/pdelab/gridfunctionspace/subspace.hh"
#include "dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh"
#include<dune/pdelab/gridfunctionspace/vectorgridfunctionspace.hh>
#include <dune/pdelab/common/vtkexport.hh>
#include <dune/pdelab/common/functionutilities.hh>
//writing the txt file plot point
#include <iostream>
#include <fstream>


#include "Integral_over_boundary.hh"

template<typename Grid, typename GV,typename FEM>
void driver(const Grid& grid, const GV& gv, FEM& fem, Dune::ParameterTree& initree, int degree)
{
    //dimesion for gv
    const int dim = GV::dimension;
    using RangeType = double;
    using DF = typename GV::ctype;
    
    double timeinterval = initree.get<double>("compression.compression_interval", 2);
    double dt = initree.get<double>("instat.dt", 0.1);
    double deformation = initree.get<double>("compression.deformation", -10);
    
    double factor_darcy = initree.get<double>("constants.factor_darcy", 0);

    int infoflag = initree.get<int>("information.infoflag", 0 );
    
    //For instationary
    double T = initree.get<double>("instat.T", 1.0);
    
    //Initializ
    double time = 0.0;
    double sum = 0.0;
    double outflow = 0.0;

    
    //GridfunctionSpaces
        //GFS für u
        using VectorBackend = Dune::PDELab::ISTL::VectorBackend<Dune::PDELab::ISTL::Blocking::none>;
        using GFS = Dune::PDELab::GridFunctionSpace<GV,FEM, Dune::PDELab::ConformingDirichletConstraints, VectorBackend>;
    
        // 3-times the Gridfunctionspace make u a vector-valued-function
        using GFS_POW2GFS = Dune::PDELab::PowerGridFunctionSpace<GFS, 3, VectorBackend, Dune::PDELab::LexicographicOrderingTag>;
    
        GFS u_gfs(gv, fem);
        u_gfs.name("u");
        
        //assign u1 u2 and u3 to the subspaces
        GFS_POW2GFS u1u2u3_gfs(u_gfs);
        u1u2u3_gfs.child(Dune::Indices::_0).name("u1");
        u1u2u3_gfs.child(Dune::Indices::_1).name("u2");
        u1u2u3_gfs.child(Dune::Indices::_2).name("u3");
    
    
        //GFS for p
        GFS p_gfs(gv, fem);
        p_gfs.name("p");
    
        //combine all
        using GFS_POW2GFS_GFS = Dune::PDELab::CompositeGridFunctionSpace<VectorBackend, Dune::PDELab::LexicographicOrderingTag, GFS_POW2GFS, GFS>;
        GFS_POW2GFS_GFS combinationGFS(u1u2u3_gfs,p_gfs);
        combinationGFS.update();
    
        //LocalOperator_r
        using LOCAL_OPERATOR_r = BiotEq_r_Operator<GFS_POW2GFS_GFS,GFS_POW2GFS_GFS>;
        LOCAL_OPERATOR_r lop_r(combinationGFS,combinationGFS,initree);
        //massOperator
        using LOCAL_OPERATOR_MASS = massOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS>;
        LOCAL_OPERATOR_MASS lop_mass(combinationGFS,combinationGFS, initree);
    
        //Gridfunction
        using BoolFunctionExpression = std::function<bool(typename GV::Intersection, typename GV::Intersection::LocalCoordinate)>;
        using BoolGridFunctionLeaf = Dune::PDELab::LocalCallableToBoundaryConditionAdapter<BoolFunctionExpression>;
    
        // Where dirichletchonditions ...
    
        // ... for u1 and u2
        BoolFunctionExpression diricletcondu12([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[2] < 1e-08 and x[0]> 8.0+ 1e-08 and x[0] < 12.0- 1e-08 and x[1] > 8.0+ 1e-08 and x[1] < 12.0-1e-08 ? 1 : 0);; });

        // ... for u3
        BoolFunctionExpression diricletcondu3([&](const auto& is, const auto& xl){ auto x=is.geometry().global(xl); return (x[2] < 1e-08 or x[2]> 19.99999999 ? 1 : 0);; });
    
        // ... for p (pores)
        BoolFunctionExpression diricletcondp([&](const auto& is, const auto& xl)
            { auto x=is.geometry().global(xl);
              return ((x[0] < 1e-08 and x[1] > 4.75+ 1e-08 and x[1] < 5.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[0] < 1e-08 and x[1] > 4.75+ 1e-08 and x[1] < 5.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[0] < 1e-08 and x[1] > 14.75+ 1e-08 and x[1] < 15.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[0] < 1e-08 and x[1] > 14.75+ 1e-08 and x[1] < 15.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[0] > 1-1e-08 and x[1]>4.75+ 1e-08 and x[1] < 5.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[0] > 1-1e-08 and x[1]>4.75+ 1e-08 and x[1] < 5.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[0] > 1-1e-08 and x[1]>14.75+ 1e-08 and x[1] < 15.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[0] > 1-1e-08 and x[1]>14.75+ 1e-08 and x[1] < 15.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[1] < 1e-08 and x[0] > 4.75+ 1e-08 and x[0] < 5.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[1] < 1e-08 and x[0] > 4.75+ 1e-08 and x[0] < 5.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[1] < 1e-08 and x[0] > 14.75+ 1e-08 and x[0] < 15.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[1] < 1e-08 and x[0] > 14.75+ 1e-08 and x[0] < 15.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[1] > 1-1e-08 and x[0] > 4.75+ 1e-08 and x[0] < 5.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[1] > 1-1e-08 and x[0] > 4.75+ 1e-08 and x[0] < 5.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                      or
                      (x[1] > 1-1e-08 and x[0] > 14.75+ 1e-08 and x[0] < 15.25- 1e-08 and x[2] > 4.75+ 1e-08 and x[2] < 5.25- 1e-08)
                      or
                      (x[1] > 1-1e-08 and x[0] > 14.75+ 1e-08 and x[0] < 15.25- 1e-08 and x[2] > 14.75+ 1e-08 and x[2] < 15.25- 1e-08)
                    ? 1 : 0.0);; });
    
        //Assign the bool values to the grid functions
        //dirichletcond u
            //dirichlet space u1
                BoolGridFunctionLeaf dirichlet_u1(diricletcondu12);
            //dirichlet space u2
                BoolGridFunctionLeaf dirichlet_u2(diricletcondu12);
            //dirichlet space u3
                BoolGridFunctionLeaf dirichlet_u3(diricletcondu3);
    
        using CompositeConstraintsParameters_u1u2u3 = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(dirichlet_u1)>,std::decay_t<decltype(dirichlet_u2)>,std::decay_t<decltype(dirichlet_u3)>>;

        CompositeConstraintsParameters_u1u2u3 composite_u1u2u3(dirichlet_u1, dirichlet_u2, dirichlet_u3);
    
        //dirichlet space p
        BoolGridFunctionLeaf dirichlet_p(diricletcondp);
            
        // whole Dirichlet-area
        using BoundaryConditionType = Dune::PDELab::CompositeConstraintsParameters<std::decay_t<decltype(composite_u1u2u3)>,std::decay_t<decltype(dirichlet_p)>>;
        BoundaryConditionType is_dirichlet(composite_u1u2u3,dirichlet_p);
        
        // Set Dirchletcon to specific values
            using FunctionExpression = std::function<RangeType(typename GV::template Codim<0>::Entity, typename GV::template Codim<0>::Entity::Geometry::LocalCoordinate)>;
            using GridFunctionLeaf = Dune::PDELab::LocalCallableToInstationaryGridFunctionAdapter<GV, RangeType, 1, FunctionExpression, LOCAL_OPERATOR_r>;
    
            //Everywhere 0
            FunctionExpression zeroOnBoundary([&](const auto& is, const auto& xl){ return 0.0; });
    
            //Compression
            FunctionExpression pushdown_time([&](const auto& is, const auto& xl){
                auto x=is.geometry().global(xl);
                double pushlevel ;
                if(time==0){
                    pushlevel = 0.0;
                }
                else if((deformation/timeinterval)*time >= deformation){
                    pushlevel = (deformation/timeinterval)*time;
                }
                else{
                    pushlevel = deformation;
                }
                return (x[2] < 1e-08 ? 0.0 : pushlevel); });
            
            //Set values
            //u1
            GridFunctionLeaf dBoundary_u1(gv, zeroOnBoundary, lop_r);
            //u2
            GridFunctionLeaf dBoundary_u2(gv,zeroOnBoundary, lop_r);
            //u3s
            GridFunctionLeaf dBoundary_u3(gv,pushdown_time, lop_r);

            //u1u2 together
            using dBOUNDARYu123 = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u1)>,std::decay_t<decltype(dBoundary_u2)>, std::decay_t<decltype(dBoundary_u3)>>;
            dBOUNDARYu123 dBoundary_u(dBoundary_u1, dBoundary_u2, dBoundary_u3);
    
            //p
            GridFunctionLeaf dBoundary_p(gv, zeroOnBoundary, lop_r);
    
            //DirichletBoundary Complete wuth the values
            using BoundaryGridFunction = Dune::PDELab::CompositeGridFunction<std::decay_t<decltype(dBoundary_u)>,std::decay_t<decltype(dBoundary_p)>>;
            BoundaryGridFunction BoundaryCondition(dBoundary_u,dBoundary_p);
   
        
        //Constrains
            //constrainscontainer
            using CONSTRAINS = typename GFS_POW2GFS_GFS::template ConstraintsContainer<RangeType>::Type;
            CONSTRAINS constrains;
            //clear
            constrains.clear();
            //Fill with constrains
            Dune::PDELab::constraints(is_dirichlet, combinationGFS, constrains);
    
        //Gridoperator
        combinationGFS.update();

        // MatrixBeackend erstellen
        using MBE = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
        MBE mbe((int)pow(1+2*degree,dim));
    
        using GRIDOPERATOR_r = Dune::PDELab::GridOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS, LOCAL_OPERATOR_r, MBE, DF, RangeType, RangeType, CONSTRAINS, CONSTRAINS>;
        using GRIDOPERATOR_mass = Dune::PDELab::GridOperator<GFS_POW2GFS_GFS, GFS_POW2GFS_GFS, LOCAL_OPERATOR_MASS, MBE, DF, RangeType, RangeType, CONSTRAINS, CONSTRAINS>;
        /*
        typedef Dune::PDELab::GridOperator<
        GFS,GFS,   ansatz and test space
        LOP,       local operator
        MBE,       matrix backend
        RF,RF,RF,  domain, range, jacobian field type
        CC,CC      constraints for ansatz and test space
        > GO;
        */
    
        GRIDOPERATOR_r go_r(combinationGFS, constrains, combinationGFS, constrains, lop_r, mbe);
        GRIDOPERATOR_mass go_mass(combinationGFS, constrains, combinationGFS, constrains, lop_mass, mbe);
    
        std::cout << "gfs with " << combinationGFS.size() << " dofs generated  "<< std::endl;
        std::cout << "cc with " << constrains.size() << " dofs generated  "<< std::endl;
        
        //ONE STEP GRID OPERATOR ---combines two gridOperators representing the spartial and temporal residual forms
        //Allows very general implementation of discretizations using the line method
        using IGO = Dune::PDELab::OneStepGridOperator<GRIDOPERATOR_r,GRIDOPERATOR_mass>;
        IGO igo(go_r, go_mass);


        //Vector
        using V_R = Dune::PDELab::Backend::Vector<GFS_POW2GFS_GFS,DF>;
        V_R x_r(combinationGFS);
    
        x_r = 0.0;
        Dune::PDELab::interpolate(BoundaryCondition, combinationGFS, x_r);

        //Solver
        using LinearSolver = Dune::PDELab::ISTLBackend_SEQ_UMFPack; //Solver backend using UMFPack as a direct solver
        //MARK: WARUM??
        using SLP = Dune::PDELab::StationaryLinearProblemSolver<IGO, LinearSolver, V_R>;
        //MARK: Weil zu jedem Zeitschritt als stationär behandelt
        LinearSolver ls(false);
        SLP slp(igo,ls,1e-8);
    
    
        //Handle instationary problem
        using TSM = Dune::PDELab::ImplicitEulerParameter<RangeType>;
        TSM tsm;
        tsm.name();
        //ONE STEP METHOD - do one step of a time stepping sheme
        using OSM = Dune::PDELab::OneStepMethod<RangeType, IGO, SLP, V_R, V_R>;
        OSM osm(tsm, igo, slp);
    
    
        //Write VTK-files
        using VTKSW = Dune::VTKSequenceWriter<GV>;
        using VTKWriter = Dune::SubsamplingVTKWriter<GV>;
        int subsampling = initree.get("subsampling.subsampling", (int) 1);  // last Parameter default Value
        Dune::RefinementIntervals subint(subsampling);
        VTKWriter vtkwriter(gv, subint);
        std::string vtkfile = initree.get<std::string>("wrapper.vtkcompare.name", "output");
        VTKSW vtkSequenceWriter(std::make_shared<VTKWriter>(vtkwriter), vtkfile);
    
        
        //Get computed Functions p and u as Gridfunction
            // p
            using Pathp = Dune::TypeTree::HybridTreePath<Dune::index_constant<1>>;
            using PSubGFS = Dune::PDELab::GridFunctionSubSpace<GFS_POW2GFS_GFS, Pathp>;
            PSubGFS psubGFS(combinationGFS);
            
            using PDGF = Dune::PDELab::DiscreteGridFunction<PSubGFS, V_R>;
            PDGF p_dgf(psubGFS, x_r);
            vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<PDGF>>(p_dgf,"p"));
            
            // u
            using Pathu = Dune::TypeTree::HybridTreePath<Dune::index_constant<0>>;
            using USubGFS = Dune::PDELab::GridFunctionSubSpace<GFS_POW2GFS_GFS, Pathu>;
            USubGFS usubGFS(combinationGFS);
             
            using UDGF = Dune::PDELab::VectorDiscreteGridFunction<USubGFS, V_R>;
            UDGF u_dgf(usubGFS, x_r);
            vtkSequenceWriter.addVertexData(std::make_shared< Dune::PDELab::VTKGridFunctionAdapter<UDGF>>(u_dgf,"u"));
    
    
    
        //Compute grad p as Gridfunction
            using GRAD_P_GF = Dune::PDELab::DiscreteGridFunctionGradient<PSubGFS,V_R>;
            GRAD_P_GF grad_p_dgf(psubGFS, x_r);
            vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<GRAD_P_GF>>(grad_p_dgf,"grad_p"));
    
    
        // Txt-file to store values for v_D on the given point (given in Line MARK: 394)
        std::ofstream outfile1 ("Values_at_Point_1.txt");
        std::ofstream outfile2 ("Values_at_Point_2.txt");
        std::ofstream outfile3 ("Values_at_Point_3.txt");
        std::ofstream outfile4 ("Values_at_Point_4.txt");
        std::ofstream outfile5 ("Values_at_Point_5.txt");
        std::ofstream outfile6 ("Values_at_Point_6.txt");
        std::ofstream outfile7 ("Values_at_Point_7.txt");
        std::ofstream outfile8 ("Values_at_Point_8.txt");
        std::ofstream outfile9 ("Values_at_Point_9.txt");
        std::ofstream outfile10 ("Values_at_Point_10.txt");
        std::ofstream outfile11 ("Values_at_Point_11.txt");
        std::ofstream outfile12 ("Values_at_Point_12.txt");
        std::ofstream outfile13 ("Values_at_Point_13.txt");
        std::ofstream outfile14 ("Values_at_Point_14.txt");
        std::ofstream outfile15 ("Values_at_Point_15.txt");
        std::ofstream outfile16 ("Values_at_Point_16.txt");
        std::ofstream outfile17 ("Values_at_Point_17.txt");
        std::ofstream outfile18 ("Values_at_Point_18.txt");
        std::ofstream outfile19 ("Values_at_Point_19.txt");
        std::ofstream outfile20 ("Values_at_Point_20.txt");
        std::ofstream outfile21 ("Values_at_Point_21.txt");
        std::ofstream outfile22 ("Values_at_Point_22.txt");
        std::ofstream outfile23 ("Values_at_Point_23.txt");
        std::ofstream outfile24 ("Values_at_Point_24.txt");
        std::ofstream outfile25 ("Values_at_Point_25.txt");
    
        //solve instationary Problem
        while(time <T-1e-8){
        
            if(time==0){
                std::cout << "Deformation: " << 0.0 << std::endl;
            }
            else if((deformation/timeinterval)*time >= deformation){
                std::cout << "Deformation: " << (deformation/timeinterval)*time << std::endl;
            }
            else{
                std::cout << "Deformation: " << deformation << std::endl;
            }

            //solve
                V_R new_coefficient(x_r);
                osm.apply(time, dt, x_r, BoundaryCondition, new_coefficient);
            //Acept new time step
                x_r = new_coefficient;
            
            //make VTK-file for timestep:
                vtkSequenceWriter.vtkWriter()->clear();
                //p
                
                vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<PDGF>>(p_dgf,"p"));
                //grad p
                GRAD_P_GF grad_p_dgf(psubGFS, x_r);
                vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<GRAD_P_GF>>(grad_p_dgf,"grad_p"));
                //u
                UDGF u_dgf(usubGFS, x_r);
                vtkSequenceWriter.addVertexData(std::make_shared<Dune::PDELab::VTKGridFunctionAdapter<UDGF>>(u_dgf,"u"));
                vtkSequenceWriter.write(time, Dune::VTK::appendedraw);

            //Evaluate -factor_darcy * grad p
            using EVALPOINT_Grad = Dune::PDELab::GridFunctionProbe<GRAD_P_GF>;
            typedef typename GRAD_P_GF::Traits::DomainType Domaingrad;
            typedef typename GRAD_P_GF::Traits::RangeType Rangegrad;
            
            Rangegrad val_grad;
            
            // EVAL Darcy at special points
            
            
                Domaingrad qp_grad1({0 , 0.9090909090909091 , 0.9090909090909091});
                outfile1 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad1(grad_p_dgf, qp_grad1);
                GridFunctionProbe_grad1.eval(val_grad,0);
    //            std::cout << "Punkt = " << qp_grad << std::endl;
                std::cout << "grad_p(Punkt) =" << val_grad << std::endl;
                //MARK: - because of the direction
                outfile1 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad2({0 , 0.9090909090909091 , 6.363636363636363});
                outfile2 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad2(grad_p_dgf, qp_grad2);
                GridFunctionProbe_grad2.eval(val_grad,0);
                outfile2 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad3({0 , 0.9090909090909091 , 10.0});
                outfile3 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad3(grad_p_dgf, qp_grad3);
                GridFunctionProbe_grad3.eval(val_grad,0);
                outfile3 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad4({0 , 0.9090909090909091 , 13.636363636363637});
                outfile4 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad4(grad_p_dgf, qp_grad4);
                GridFunctionProbe_grad4.eval(val_grad,0);
                outfile4 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad5({0 , 0.9090909090909091 , 19.09090909090909});
                outfile5 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad5(grad_p_dgf, qp_grad5);
                GridFunctionProbe_grad5.eval(val_grad,0);
                outfile5 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad6({0 , 6.363636363636363 , 0.9090909090909091});
                outfile6 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad6(grad_p_dgf, qp_grad6);
                GridFunctionProbe_grad6.eval(val_grad,0);
                outfile6 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad7({0 , 6.363636363636363 , 6.363636363636363});
                outfile7 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad7(grad_p_dgf, qp_grad7);
                GridFunctionProbe_grad7.eval(val_grad,0);
                outfile7 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad8({0 , 6.363636363636363 , 10.0});
                outfile8 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad8(grad_p_dgf, qp_grad8);
                GridFunctionProbe_grad8.eval(val_grad,0);
                outfile8 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad9({0 , 6.363636363636363 , 13.636363636363637});
                outfile9 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad9(grad_p_dgf, qp_grad9);
                GridFunctionProbe_grad9.eval(val_grad,0);
                outfile9 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad10({0 , 6.363636363636363 , 19.09090909090909});
                outfile10 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad10(grad_p_dgf, qp_grad10);
                GridFunctionProbe_grad10.eval(val_grad,0);
                outfile10 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad11({0 , 10.0 , 0.9090909090909091});
                outfile11 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad11(grad_p_dgf, qp_grad11);
                GridFunctionProbe_grad11.eval(val_grad,0);
                outfile11 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad12({0 , 10.0 , 6.363636363636363});
                outfile12 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad12(grad_p_dgf, qp_grad12);
                GridFunctionProbe_grad12.eval(val_grad,0);
                outfile12 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad13({0 , 10.0 , 10.0});
                outfile13 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad13(grad_p_dgf, qp_grad13);
                GridFunctionProbe_grad13.eval(val_grad,0);
                outfile13 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad14({0 , 10.0 , 13.636363636363637});
                outfile14 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad14(grad_p_dgf, qp_grad14);
                GridFunctionProbe_grad14.eval(val_grad,0);
                outfile14 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad15({0 , 10.0 , 19.09090909090909});
                outfile15 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad15(grad_p_dgf, qp_grad15);
                GridFunctionProbe_grad15.eval(val_grad,0);
                outfile15 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad16({0 , 13.636363636363637 , 0.9090909090909091});
                outfile16 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad16(grad_p_dgf, qp_grad16);
                GridFunctionProbe_grad16.eval(val_grad,0);
                outfile16 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad17({0 , 13.636363636363637 , 6.363636363636363});
                outfile17 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad17(grad_p_dgf, qp_grad17);
                GridFunctionProbe_grad17.eval(val_grad,0);
                outfile17 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad18({0 , 13.636363636363637 , 10.0});
                outfile18 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad18(grad_p_dgf, qp_grad18);
                GridFunctionProbe_grad18.eval(val_grad,0);
                outfile18 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad19({0 , 13.636363636363637 , 13.636363636363637});
                outfile19 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad19(grad_p_dgf, qp_grad19);
                GridFunctionProbe_grad19.eval(val_grad,0);
                outfile19 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad20({0 , 13.636363636363637 , 19.09090909090909});
                outfile20 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad20(grad_p_dgf, qp_grad20);
                GridFunctionProbe_grad20.eval(val_grad,0);
                outfile20 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad21({0 , 19.09090909090909 , 0.9090909090909091});
                outfile21 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad21(grad_p_dgf, qp_grad21);
                GridFunctionProbe_grad21.eval(val_grad,0);
                outfile21 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad22({0 , 19.09090909090909 , 6.363636363636363});
                outfile22 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad22(grad_p_dgf, qp_grad22);
                GridFunctionProbe_grad22.eval(val_grad,0);
                outfile22 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad23({0 , 19.09090909090909 , 10.0});
                outfile23 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad23(grad_p_dgf, qp_grad23);
                GridFunctionProbe_grad23.eval(val_grad,0);
                outfile23 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad24({0 , 19.09090909090909 , 13.636363636363637});
                outfile24 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad24(grad_p_dgf, qp_grad24);
                GridFunctionProbe_grad24.eval(val_grad,0);
                outfile24 << -factor_darcy*val_grad[0] << std::endl;

                Domaingrad qp_grad25({0 , 19.09090909090909 , 19.09090909090909});
                outfile25 << time << std::endl;
                EVALPOINT_Grad GridFunctionProbe_grad25(grad_p_dgf, qp_grad25);
                GridFunctionProbe_grad25.eval(val_grad,0);
                outfile25 << -factor_darcy*val_grad[0] << std::endl;
                
     
            
            //Compte the outflow over Time
            sum = integrateGridFunction_boundary(grad_p_dgf,1, factor_darcy, infoflag);
            if(infoflag == 1){
                std::cout << "SUM = " << sum << std::endl;
            }
            outflow = outflow + sum*dt;
            if(infoflag == 1){
                std::cout << "OUTFLOW = " << outflow << std::endl;
            }
            
            //next time step
            time += dt;
        }
    
    std::cout << "Flow out of the pores: " << outflow << std::endl;
    outfile1.close();
    outfile2.close();
    outfile3.close();
    outfile4.close();
    outfile5.close();
    outfile6.close();
    outfile7.close();
    outfile8.close();
    outfile9.close();
    outfile10.close();
    outfile11.close();
    outfile12.close();
    outfile13.close();
    outfile14.close();
    outfile15.close();
    outfile16.close();
    outfile17.close();
    outfile18.close();
    outfile19.close();
    outfile20.close();
    outfile21.close();
    outfile22.close();
    outfile23.close();
    outfile24.close();
    outfile25.close();
    
}

#endif //BIOTEQUATIONS_STATIONARY_DRIVERBLOCK_HH
